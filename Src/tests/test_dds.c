
#include "structures.h"
#include "../system/periphs/all.h"
#include "tests/test_dds.h"

//#define DDS_DUMP_REGISTERS

void DDS_Init();
HAL_StatusTypeDef DDS_WriteRegister(const PRegister reg, void* data);
HAL_StatusTypeDef DDS_ReadRegister(const PRegister reg, void* data);
uint32_t DDS_CountFTWRegisterValue(uint32_t frequency);
void DDS_DumpRegisters();
uint8_t* DDS_ConvertUint32toUint8Array(uint32_t value);


void DDS_test_100Hz();
void DDS_test_1kHz();
void DDS_test_10kHz();
void DDS_test_100kHz();
void DDS_test_500kHz();
void DDS_test_1MHz();
void DDS_test_5MHz();

void DDS_test_PowerDown(){
	HAL_GPIO_WritePin(DDS_PWR_EN_GPIO_Port, DDS_PWR_EN_Pin, GPIO_PIN_RESET);
}

void DDS_test_100Hz(){
	HAL_StatusTypeDef result = HAL_OK;
	printf("DDS Test 100Hz:\n");
#ifdef DDS_DUMP_REGISTERS
	DDS_DumpRegisters();
#endif

	uint32_t frequency = DDS_CountFTWRegisterValue(100);
	uint8_t* frequencyData = DDS_ConvertUint32toUint8Array(frequency);
	result = DDS_WriteRegister(DDS_FTW, frequencyData);

	if(result != HAL_OK){
		printf("DDS Test 100Hz Set Frequency fail! %d\n", result);
	}
#ifdef DDS_DUMP_REGISTERS
	DDS_DumpRegisters();
#endif

	printf("DDS Test 100Hz finished!\n");
}

void DDS_test_1kHz(){
	HAL_StatusTypeDef result = HAL_OK;
	printf("DDS Test 1kHz:\n");
#ifdef DDS_DUMP_REGISTERS
	DDS_DumpRegisters();
#endif

	uint32_t frequency = DDS_CountFTWRegisterValue(1000);
	uint8_t* frequencyData = DDS_ConvertUint32toUint8Array(frequency);
	result = DDS_WriteRegister(DDS_FTW, frequencyData);

	if(result != HAL_OK){
		printf("DDS Test 1kHz Set Frequency fail! %d\n", result);
	}
#ifdef DDS_DUMP_REGISTERS
	DDS_DumpRegisters();
#endif

	printf("DDS Test 1kHz finished!\n");
}

void DDS_test_10kHz(){
	HAL_StatusTypeDef result = HAL_OK;
	printf("DDS Test 10kHz:\n");
#ifdef DDS_DUMP_REGISTERS
	DDS_DumpRegisters();
#endif
	uint32_t frequency = DDS_CountFTWRegisterValue(10000);
	uint8_t* frequencyData = DDS_ConvertUint32toUint8Array(frequency);
	result = DDS_WriteRegister(DDS_FTW, frequencyData);

	if(result != HAL_OK){
		printf("DDS Test 10kHz Set Frequency fail! %d\n", result);
	}
#ifdef DDS_DUMP_REGISTERS
	DDS_DumpRegisters();
#endif

	printf("DDS Test 10kHz finished!\n");
}

void DDS_test_100kHz(){
	HAL_StatusTypeDef result = HAL_OK;
	printf("DDS Test 100kHz:\n");
#ifdef DDS_DUMP_REGISTERS
	DDS_DumpRegisters();
#endif

	uint32_t frequency = DDS_CountFTWRegisterValue(100000);
	uint8_t* frequencyData = DDS_ConvertUint32toUint8Array(frequency);
	result = DDS_WriteRegister(DDS_FTW, frequencyData);

	if(result != HAL_OK){
		printf("DDS Test 100kHz Set Frequency fail! %d\n", result);
	}
#ifdef DDS_DUMP_REGISTERS
	DDS_DumpRegisters();
#endif

	printf("DDS Test 100kHz finished!\n");
}

void DDS_test_500kHz(){
	HAL_StatusTypeDef result = HAL_OK;
	printf("DDS Test 500kHz:\n");
#ifdef DDS_DUMP_REGISTERS
	DDS_DumpRegisters();
#endif

	uint32_t frequency = DDS_CountFTWRegisterValue(500000);
	uint8_t* frequencyData = DDS_ConvertUint32toUint8Array(frequency);
	result = DDS_WriteRegister(DDS_FTW, frequencyData);

	if(result != HAL_OK){
		printf("DDS Test 500kHz Set Frequency fail! %d\n", result);
	}
#ifdef DDS_DUMP_REGISTERS
	DDS_DumpRegisters();
#endif

	printf("DDS Test 500kHz finished!\n");
}

void DDS_test_1MHz(){
	HAL_StatusTypeDef result = HAL_OK;
	printf("DDS Test 1MHz:\n");
#ifdef DDS_DUMP_REGISTERS
	DDS_DumpRegisters();
#endif

	uint32_t frequency = DDS_CountFTWRegisterValue(1000000);
	uint8_t* frequencyData = DDS_ConvertUint32toUint8Array(frequency);
	result = DDS_WriteRegister(DDS_FTW, frequencyData);

	if(result != HAL_OK){
		printf("DDS Test 1MHz Set Frequency fail! %d\n", result);
	}
#ifdef DDS_DUMP_REGISTERS
	DDS_DumpRegisters();
#endif

	printf("DDS Test 1MHz finished!\n");
}

void DDS_test_5MHz(){
	HAL_StatusTypeDef result = HAL_OK;
	printf("DDS Test 5MHz:\n");
#ifdef DDS_DUMP_REGISTERS
	DDS_DumpRegisters();
#endif

	uint32_t frequency = DDS_CountFTWRegisterValue(5000000);
	uint8_t* frequencyData = DDS_ConvertUint32toUint8Array(frequency);
	result = DDS_WriteRegister(DDS_FTW, frequencyData);

	if(result != HAL_OK){
		printf("DDS Test 5MHz Set Frequency fail! %d\n", result);
	}
#ifdef DDS_DUMP_REGISTERS
	DDS_DumpRegisters();
#endif

	printf("DDS Test 5MHz finished!\n");
}


