#include "main.h"
#include "periphs/all.h"
#include "wifi_module.h"
extern __IO IO_status_flag_typedef IO_status_flag;

const uint32_t KEEP_POWER_FOR = (60 * 1024 * 10); // 2 minutes

DeviceStatus deviceStatus;
extern volatile uint8_t wifi_client_connected;

void system_main() {
	WIFI_LED_OnOff(1);
	deviceStatus.state = deviceStatus.state = PREINIT;
	// enable charging
	HAL_Delay(100);
	HAL_Delay(100);
	HAL_GPIO_WritePin(CHARGER_EN1_GPIO_Port, CHARGER_EN1_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(CHARGER_EN2_GPIO_Port, CHARGER_EN2_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(CHARGER_CE_GPIO_Port, CHARGER_CE_Pin, GPIO_PIN_RESET);


	volatile int scanButton = HAL_GPIO_ReadPin(USER_SW_nSCAN_GPIO_Port, USER_SW_nSCAN_Pin);
	volatile int powerButton = HAL_GPIO_ReadPin(USER_SW_nPOWER_GPIO_Port, USER_SW_nPOWER_Pin);
	// wait for power on
	GPIO_PinState isPwrSw;
	SysTick->CTRL &= ~1;
	SysTick->CTRL |= 1;
	deviceStatus.state = INIT;
	deviceStatus.requestRestart = 0;
	//initialize device
	power_OnStart();


	HAL_GPIO_WritePin(AFE_PWR_EN_GPIO_Port, AFE_PWR_EN_Pin, GPIO_PIN_SET);
	deviceStatus.state = CONF;
	data_Reload();

	MAX_Init();
	updateBatteryState();
	MAX_Dump();

	ACC_Init();
	ADC_Init();
	if (scanButton == 0 && powerButton == 0) {
//		uint32_t start = HAL_GetTick();
//		do {
//			scanButton = HAL_GPIO_ReadPin(USER_SW_nSCAN_GPIO_Port, USER_SW_nSCAN_Pin);
//			powerButton = HAL_GPIO_ReadPin(USER_SW_nPOWER_GPIO_Port, USER_SW_nPOWER_Pin);
//		} while(HAL_GetTick() - start < 2000 || (scanButton == 0 && powerButton == 0));
//
//		if (scanButton == 0 && powerButton == 0) {
			data_ClearWiFiConfig();
//		}
	}
	deviceStatus.state = IDLE;

//	MAX_Dump();
	power_KeepAlive();
	// start wifi

	deviceConfig.accFrequency = 0;
	deviceConfig.adcFrequency = 12.5f;

	HAL_NVIC_SetPriority(USART1_IRQn, 0, 0);
	USART1->CR1 &= ~USART_CR1_UE;;
	USART1->CR1 |= USART_CR1_RXNEIE;
	USART1->CR1 &= ~USART_CR1_IDLEIE;
	USART1->CR3 |= USART_CR3_OVRDIS;
	USART1->CR1 |= USART_CR1_UE;;
	NVIC_ClearPendingIRQ(USART1_IRQn);
//	HAL_UART_Transmit(&huart1, "Hello!\r\n", 8, 10);
	NVIC_EnableIRQ(USART1_IRQn);

	WIFI_LED_OnOff(0);
	WIFI_Main();
}

uint32_t lastBatteryUpdate = 0;
uint32_t lastBatteryLedBlink = 0;
uint32_t lastWifiLedBlink = 0;

void ledUpdate () {

}

void system_Tick() {
	uint32_t now = HAL_GetTick();
	// check power sw
	GPIO_PinState isPwrSw = HAL_GPIO_ReadPin(USER_SW_nPOWER_GPIO_Port,
			USER_SW_nPOWER_Pin);

	if (deviceStatus.requestRestart != 0) {
		int32_t now = HAL_GetTick();
		if ((int32_t)(now - deviceStatus.requestRestart) > 0) {
			power_Off();

			HAL_GPIO_WritePin(PWR_KEEP_GPIO_Port, PWR_KEEP_Pin, GPIO_PIN_RESET);
			HAL_Delay(20);
			NVIC_SystemReset();
		}
	}

	if (isPwrSw == GPIO_PIN_RESET && deviceStatus.powerTimeout == 0) {
		deviceStatus.powerTimeout = now;
	} else if (isPwrSw == GPIO_PIN_SET) {
		//deviceStatus.powerTimeout = 0;
	}



	if (deviceStatus.state == IDLE) {
		static uint32_t lastLedOn = 0;
		if (wifi_client_connected) {
			if (HAL_GetTick() - lastLedOn > 1024) {
				HAL_GPIO_WritePin(USER_LED_SCAN_GPIO_Port, USER_LED_SCAN_Pin, GPIO_PIN_SET);
				lastLedOn = HAL_GetTick();
			} else if (HAL_GetTick() - lastLedOn > 128) {
				HAL_GPIO_WritePin(USER_LED_SCAN_GPIO_Port, USER_LED_SCAN_Pin, GPIO_PIN_RESET);
			}
		}

	}

	{ //battery LED
		switch (batteryStatus.status) {
		case EMPTY:
		case LOW:
			if (HAL_GetTick() - lastBatteryLedBlink > 1024) {
				HAL_GPIO_TogglePin(USER_LED_BAT_GPIO_Port, USER_LED_BAT_Pin);
				lastBatteryLedBlink = HAL_GetTick();
			}
			break;
		case GOOD: // nothing
			HAL_GPIO_WritePin(USER_LED_BAT_GPIO_Port, USER_LED_BAT_Pin,
					GPIO_PIN_RESET);
			break;
		case CHARGING: // fast blinking
			if (HAL_GetTick() - lastBatteryLedBlink > 512) {
				HAL_GPIO_TogglePin(USER_LED_BAT_GPIO_Port, USER_LED_BAT_Pin);
				lastBatteryLedBlink = HAL_GetTick();
			}
			break;
		case FULL: //light
			HAL_GPIO_WritePin(USER_LED_BAT_GPIO_Port, USER_LED_BAT_Pin,
					GPIO_PIN_SET);
			break;
		}
	}

}

uint8_t WIFI_SendBuffer[1024];
uint32_t WIFI_SendSize;


void system_SendPendingFrames() {
	WIFI_SendSize = 0;
	int sending = 0;

	static uint32_t lastSend = 0;
	volatile uint32_t now = HAL_GetTick();
	volatile int diff = now - lastSend;

	if (diff < 500) {
		return;
	}
	lastSend = now;
	while (1) {
		volatile struct FrameToSend ready = frames_GetReadyFrame();
		if (ready.size == 0 || ready.frame == 0 || ready.frame->function == 0) {
			break;
		}
		sending = 1;

		if (WIFI_SendSize + ready.size > 1000) {
			break;
		}

#ifdef DEBUG_PRINT
		printf("Add frame %p [%d, size=%d;%d ; %d]\n", ready.frame, ready.frame->function, ready.size, ready.frame->frameSize, WIFI_SendSize);
#endif
		__disable_irq();
		memcpy(WIFI_SendBuffer + WIFI_SendSize, ready.frame, ready.size);
		WIFI_SendSize += ready.size;
		memset(ready.frame, 0, ready.size);
		ready.timestamp = 0;
		ready.size = 0;
		ready.frame = 0;
		__enable_irq();
	}
	if (WIFI_SendSize > 0) {
#ifdef DEBUG_PRINT
		printf("Send %d bytes\n", WIFI_SendSize);
#endif

//		HAL_UART_Transmit(&huart1, WIFI_SendBuffer, WIFI_SendSize, 10);

		if (wifi_client_connected) {
			while (!IO_status_flag.command_mode) {
				__NOP();
			};
			WIFI_ExecuteCommand("AT\r");
			WiFi_response(WIFI_SendBuffer, WIFI_SendSize);
			HAL_Delay(50);
		}
	}
}

void StartScan() {
#ifdef DEBUG_PRINT
	fprintf(2, "Start\n");
#endif
	buffers_clear();
	HAL_GPIO_WritePin(USER_LED_SCAN_GPIO_Port, USER_LED_SCAN_Pin, GPIO_PIN_SET);
	ACC_Start();
	ADC_Start();
}

void StopScan() {
	ACC_Stop();
	ADC_Stop();
	HAL_GPIO_WritePin(USER_LED_SCAN_GPIO_Port, USER_LED_SCAN_Pin,
			GPIO_PIN_RESET);
	MAX_Dump();
}

inline void irqScanButton() {
	power_KeepAlive();
	if (HAL_GPIO_ReadPin(USER_SW_nSCAN_GPIO_Port, USER_SW_nSCAN_Pin)
			== GPIO_PIN_RESET) {
		// pressed
		if (deviceStatus.state == SCAN) {
			printf("Wrong device state\r\n");
		} else {
			deviceStatus.state = SCAN;
			deviceStatus.scanButtonPressed = 0;
			printf("Scan btn pressed\r\n");
			StartScan();
		}
	} else {
		MAX_Store();
		// released
		if (deviceStatus.state == IDLE) {
			printf("Wrong device state\r\n");
		} else {
			deviceStatus.state = IDLE;
			deviceStatus.scanButtonPressed = HAL_GetTick();
			printf("Scan btn released\r\n");
			StopScan();
		}
	}
}

inline void irqPowerButton() {
	if (HAL_GPIO_ReadPin(USER_SW_nPOWER_GPIO_Port, USER_SW_nPOWER_Pin)
			== GPIO_PIN_RESET) {
		// pressed
		printf("Power btn pressed\r\n");
		deviceStatus.scanButtonPressed = HAL_GetTick();
		power_KeepAlive();
	} else {
		// released
		printf("Power btn released\r\n");
//		deviceStatus.powerButtonPressed;
		power_KeepAlive();
	}
}

/**
 * @brief EXTI handlers
 * handles buttons and interruptions
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	switch (GPIO_Pin) {
	case GPIO_PIN_0:
		// I2C_nIRQ_ACC1
		ACC_IrqHandler();
		break;
	case GPIO_PIN_1:
		// I2C_nIRQ_ACC2
		ACC_IrqHandler();
		break;
	case USER_SW_nSCAN_Pin:
		if (deviceStatus.state >= IDLE) {
			irqScanButton();
		}
		break;
	case USB_DETECT_Pin:
		updateBatteryState();
		power_KeepAlive();
	}
}
void wifiIdleTask () {
	if (HAL_GetTick() - lastBatteryUpdate > 1024 * 30) {
		updateBatteryState();
		lastBatteryUpdate = HAL_GetTick();
	}

	uint32_t now = HAL_GetTick();
	// is timeout
	if (deviceStatus.state <= IDLE) {
		if (now - deviceStatus.powerTimeout > KEEP_POWER_FOR) {
#ifdef DEBUG_PRINT
			printf("[power] is timeout\n");
#endif
			power_TimeOut();
		}
	}
	// forced power off?
	if (deviceStatus.powerButtonPressed) {
		if (now - deviceStatus.powerButtonPressed > 3000) {
			power_TimeOut();
		}
	}

	system_SendPendingFrames();
}

