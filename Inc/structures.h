/*
 * structures.h
 *
 *  Created on: 07.05.2018
 *      Author: jkonieczny
 */

#ifndef STRUCTURES_H_
#define STRUCTURES_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

/**
 * @brief Function-interface to send data
 */
typedef int (*COM_Tx)(void *data, int size);

enum FrameType {
	FRAME_ReadData = 0x01,
	FRAME_WriteData = 0x02,
	FRAME_MeasureData = 0x10,
	FRAME_SysCall = 0x04,
	FRAME_TestFrame = 0x05,
	FRAME_ReadDataResponse = 0x81,
	FRAME_WriteDataResponse = 0x82,
	FRAME_SysCallResponse = 0x84,
	FRAME_TestFrameResponse = 0x85
};


enum StructureType{
	Structure_WiFiConfig = 0x0f
};

enum SysCallFunction{
	SysCall_Beep = 0x01,
	SysCall_BeepLEDs = 0x02,
	SysCall_Reset = 0x03
};

enum SysCallResponseType{
	SysCall_ResponseOK = 0x00,
	SysCall_ResponseUnknownFrameType = 0x01
};

/**
 * @brief Common frame format
 *
 */
typedef struct {
	uint8_t function;
	uint16_t frameSize;
	union { /**< optional fields */
		//uint8_t data[0]; /**< token for debugging */
		uint8_t structId;
		uint8_t sysCallFunction;

		struct { /**< in case of `function` equal to 0x01 or 0x02 */
			uint8_t structId;
			uint16_t structSize;
			uint8_t structData[16];
		}__attribute__((packed)) readWrite;

		struct { /**< in case of `function` equal to 0x82 */
			uint8_t structId;
			uint8_t status;
		}__attribute__((packed)) writeResponse;


		struct { /**< in case of `function` equal to 0x10 */
			uint32_t timestamp;
			uint16_t delta;
			uint16_t count;
			uint8_t type;
		}__attribute__((packed)) measurement;

		struct{
			char dummy[5]; // for readWrite struct
			char ssid[32];
			char password[32];
			uint16_t flags;
		}__attribute__((packed)) wifiConfig;

		struct{
			uint16_t responseSize;
			uint8_t responseData[64];
		}__attribute__((packed)) testFrame;

	};
}__attribute__((packed)) Frame;


/**
 * @brief Frame with measurements
 * max size: 132 bytes
 */
typedef struct MeasurementFrame {
	// common
	uint8_t function;
	uint16_t frameSize;
	//
	uint32_t timestamp;
	uint16_t delta;
	uint16_t count;
	uint8_t type;
	union {
		int16_t values[20][3];
	};
}__attribute__((packed)) MeasurementFrame;

/**
 * @brief Header of measurements
 */
typedef struct MeasurementFrameHeader {
	// common
	uint8_t function;
	uint16_t frameSize;
	//
	uint32_t timestamp;
	uint16_t delta;
	uint16_t count;
	uint8_t type;
}__attribute__((packed)) MeasurementFrameHeader;

/**
 * @brief [0x0F] WiFi configuration frame
 *
 * Frame to configure WiFi's SSID, password and flags
 * Flags:
 * bits [0:2] - Encryption values:
 *	0 - None,
 *	1 - WEP,
 *	2 - WPA/WPA2
 */
typedef struct WiFiConfig {
	char ssid[32];
	char password[32];
	uint16_t flags;
}__attribute__((packed)) WiFiConfig;
#define WIFI_FLAGS_MASK_PRIV_MODE	0x0003

enum WifiConfigResponseStatus{
	WifiConfigResponseOK = 0x00,
	WifiConfigResponseNetworkNotFound = 0x01,
	WifiConfigResponseEncryptionError = 0x02,
	WifiConfigModuleError = 0x03
};

typedef struct SysCallResponseFrame{
	uint8_t frameId;
	uint8_t frameSize;
	uint8_t responseCode;
}__attribute__((packed)) SysCallResponseFrame;

/**
 * @brief [0x20] Test frame structure
 */
typedef struct TestFrame{ // from device to PC
	uint8_t frameId;
	uint16_t frameSize;
	uint8_t frameData[2048];
}__attribute__((packed)) TestFrame;

/**
 * @brief [0x04] Attenuator configuration frame
 */
typedef struct {
	uint8_t value;
}__attribute__((packed)) AttenuatorConfig;

/**
 * @brief [0x03] DDS configuration frame
 */
typedef struct AnalogMeasurementConfig {
	uint32_t frequency;
	uint8_t attenuator;
	//optional?
//	uint32_t sweepParameter1;
//	uint32_t sweepParameter2;
//	uint32_t risingDelta;
//	uint32_t fallingDelta;
//	uint32_t risingSweepRampRate;
//	uint32_t fallingSweepRampRate;
//	uint16_t phaseOffset;
}__attribute__((packed)) AnalogMeasurementConfig;

extern AnalogMeasurementConfig analogConfig;

/**
 * @brief [0x03] DDS configuration frame
 */
typedef struct DeviceConfig {
	uint8_t accFrequency;
	float adcFrequency;
}__attribute__((packed)) DeviceConfig;

extern DeviceConfig deviceConfig;


/**
 * @brief Battery inner data frame
 *
 * Frame to configure WiFi's SSID, password and flags
 */
typedef union BatteryData {
	struct {
		uint16_t FullCap;
		uint16_t Cycles;
		uint16_t RCOMP0;
		uint16_t TempCo;
		uint16_t QResidual00;
		uint16_t QResidual10;
		uint16_t QResidual20;
		uint16_t QResidual30;

		uint16_t dQacc;
		uint16_t dPacc;
	};
	uint16_t _regs[10];

}__attribute__((packed)) BatteryData;

extern BatteryData batteryData;

enum BatteryStatusEnum {
	EMPTY,
	LOW,
	GOOD,
	CHARGING,
	FULL
};
/**
 * @brief [0x02] Battery status
 */
typedef struct BatteryStatus {
	uint16_t voltage;
	uint16_t avgVoltage;
	uint16_t current;
	uint16_t avgCurrent;
	uint8_t percentage;
	uint8_t status;
}__attribute__((packed)) BatteryStatus;
extern BatteryStatus batteryStatus;

/**
 * @brief [0x82] Write data response
 */
typedef struct WriteDataResponseFrame { // Size 4 byte
	uint8_t id; // 0x82
	uint16_t frameSize;
	uint8_t status;
}__attribute__((packed)) WriteDataResponseFrame;


// declarations
extern AttenuatorConfig attConfig;
extern WiFiConfig wifiConfig;
extern BatteryStatus batteryStatus;


typedef struct FlashFragment {
	void *ptr;
	uint32_t blockLength;
	uint32_t flashOffset;
	uint32_t salt;
} FlashFragment ;


#ifdef __cplusplus
}
#endif


#endif /* STRUCTURES_H_ */
