#include <stdint.h>
#include <i2c.h>
#include <gpio.h>
#include <measurements.h>
#include "structures.h"

#include "all.h"
#include "utils.h"

#define Address 0b01101100

BatteryStatus batteryStatus;

inline HAL_StatusTypeDef MAX_WriteRegister(PRegister reg, void *data) {
	return HAL_I2C_Mem_Write(&hi2c1, Address, reg.address, 1, data, reg.length,
			100);
}
inline HAL_StatusTypeDef MAX_WriteData(PRegister reg, uint16_t data) {
	return HAL_I2C_Mem_Write(&hi2c1, Address, reg.address, 1, &data, 2, 100);
}

inline HAL_StatusTypeDef MAX_ReadRegister(PRegister reg, void *data) {
	return HAL_I2C_Mem_Read(&hi2c1, Address, reg.address, 1, data, reg.length,
			100);
}

HAL_StatusTypeDef MAX_Init() {
	volatile HAL_StatusTypeDef result = HAL_OK;

	struct MaxStatusRegister status;
	MAX_ReadRegister(MAX_STATUS, &status);
#ifdef DEBUG_PRINT
	if (status.PoweronReset == 0) {
		printf("[BAT] Configuration safe\n");
		//return HAL_OK;
	} else {
		printf("[BAT] Configuration clear\n");
	}
#endif
	HAL_Delay(500);

	MAX_ReadRegister(MAX_STATUS, &status);

	MAX_WriteData(MAX_CONFIG, 0x2350);

//	result |= MAX_WriteData(MAX_TGAIN, 0xE3E1);
//	HAL_Delay(10);
//	result |= MAX_WriteData(MAX_TOFF, 0x290E);
//	HAL_Delay(10);
	union MaxConfigRegister config;
	result |= MAX_ReadRegister(MAX_CONFIG, &config);
//	config.raw = 0x2350;
	config._0 = 0;
	config.Ber = 0; //alert on battery removal
	config.Bei = 0; //alert on battery insertion
	config.Aen = 0; //Alerts enabled
	config.FTHRM = 0; //thermistor bias switch
	config.ETHRM = 1; //enable thermistor
	config.ALSH = 0; // clear Aen,
	config.I2CSH = 1; //disable I2C
	config.SHDN = 0; // shutdown
	config.Tex = 0; // temperature external
	config.Ten = 1; // temperature on AIN pin
	config.AINSH = 0; //disable AIN
	config.ALRTp = 0; //alert active low
	config.Vs = 1; //clear voltage alert automaticly
	config.Ts = 1;
	config.Ss = 1;

	status.PoweronReset = 0;
	MAX_WriteRegister(MAX_STATUS, &status);
	MAX_WriteData(MAX_FILTER_CFG, 0x87A4);
	MAX_WriteData(MAX_FULL_SOC_THR, 0x5f00); // 95% = max
	MAX_WriteData(MAX_DESIGN_CAP, lround(2250 / 0.5f)); // 95% = max
	{
		int VE = 3400/40;
		int VR = 3500/10;
		MAX_WriteData(MAX_V_EMPTY, (VE<<7) | (VR));
	}
	HAL_Delay(200);

	union MaxConfigRegister confirm;
	int tries = 10;
	do {
		result |= MAX_WriteRegister(MAX_CONFIG, &config.raw);
		HAL_Delay(4);
		confirm.raw = 0;
		result |= MAX_ReadRegister(MAX_CONFIG, &confirm.raw);
		tries--;
		if (tries == 0) {
			break;
		}
	} while (confirm.raw != config.raw);

	MAX_Restore();

#ifdef DEBUG_PRINT
	if (confirm.raw != config.raw) {
		char *buf = "[BAT] Configuration write failed, why?\n";
//		HAL_UART_Transmit(&huart1, buf, strlen(buf), 100);
	}
#endif
	return result;
}

HAL_StatusTypeDef MAX_Dump() {
#ifdef DEBUG_PRINT
	volatile GPIO_PinState chargeStatus = HAL_GPIO_ReadPin(
			CHARGER_CHG_GPIO_Port, CHARGER_CHG_Pin);
	volatile GPIO_PinState pgood = HAL_GPIO_ReadPin(CHARGER_PG_GPIO_Port,
			CHARGER_PG_Pin);

	struct MaxStatusRegister status;
	MAX_ReadRegister(MAX_STATUS, &status);

	union MaxConfigRegister config;
	MAX_ReadRegister(MAX_CONFIG, &config);

	uint16_t vfocv = 0;
	MAX_ReadRegister(MAX_VFOCV, &vfocv);
	volatile float f = (vfocv >> 4) * 1.25;

	uint16_t soc = 0;
	MAX_ReadRegister(MAX_SOC_MIX, &soc);
	volatile float fsoc = (soc >> 8) + (soc & 0xff) * 0.0039;

	uint16_t full = 0;
	MAX_ReadRegister(MAX_FULL_CAP, &full);
	volatile float ffull = full * 0.5;

	uint16_t vain = 0;
	MAX_ReadRegister(MAX_AIN, &vain);
	volatile float fain = (vain >> 3) * 0.0122;

	uint16_t vcell = 0;
	MAX_ReadRegister(MAX_VCELL, &vcell);
	volatile float fvcell = (vcell >> 3) * 0.625;

	uint16_t current = 0;
	MAX_ReadRegister(MAX_AVG_CURRENT, &current);
	volatile float fcurrent = current * (1.5625f / 0.01f) / 10000.0f;

	uint16_t vempty = 0;
	MAX_ReadRegister(MAX_V_EMPTY, &vempty);
	volatile float vE1 = (vempty >> 7) * 0.04;
	volatile float vER2 = (vempty & 0x127) * 0.01;

	int16_t temperature = 0;
	MAX_ReadRegister(MAX_TEMPERATURE, &temperature);
	volatile float ftemp = ((temperature >> 8) & 0x7f);
	ftemp += (temperature & 0xff) * 0.0039f;
	if (temperature & 0x8000) {
		ftemp *= -1;
	}

	char buf[32];

//	sprintf(buf, "[MAX] temperature: %.3fC\r\n", ftemp);
//	HAL_UART_Transmit(&huart1, buf, strlen(buf), 100);
//	sprintf(buf, "[MAX] fullCap: %.3f mAh\r\n", ffull);
//	HAL_UART_Transmit(&huart1, buf, strlen(buf), 100);
//	sprintf(buf, "[MAX] vcell: %.3fmV\r\n", fvcell);
//	HAL_UART_Transmit(&huart1, buf, strlen(buf), 100);
//	sprintf(buf, "[MAX] current: %.3fmAh\r\n", fcurrent);
//	HAL_UART_Transmit(&huart1, buf, strlen(buf), 100);
//	sprintf(buf, "[MAX] soc: %.3f%%\r\n", fsoc);
//	HAL_UART_Transmit(&huart1, buf, strlen(buf), 100);
//	fflush(stdout);
#endif
	return HAL_OK;
}

void MAX_Store() {
	volatile HAL_StatusTypeDef result = HAL_OK;
	volatile int count = sizeof(MAX_STORE) / sizeof(MAX_STORE[0]);
	for (int i = 0; i < count; i++) {
		const PRegister *reg = MAX_STORE[i];
		result |= MAX_ReadRegister(*reg, &batteryData._regs[i]);
	}
	if (result != HAL_OK){
		printf("[MAX] Couldn't get data\r\n");
	}
}

void MAX_Restore() {
	volatile HAL_StatusTypeDef result = HAL_OK;
	volatile int count = sizeof(MAX_STORE) / sizeof(MAX_STORE[0]);
	for (int i = 0; i < count; i++) {
		const PRegister *reg = MAX_STORE[i];
		result |= MAX_WriteRegister(*reg, &batteryData._regs[i]);
	}
	if (result != HAL_OK){
		printf("[MAX] Couldn't restore data\r\n");
	}
}

void updateBatteryState() {
	volatile GPIO_PinState chargeStatus = !HAL_GPIO_ReadPin(
			CHARGER_CHG_GPIO_Port, CHARGER_CHG_Pin);
	volatile GPIO_PinState pgood = !HAL_GPIO_ReadPin(CHARGER_PG_GPIO_Port,
			CHARGER_PG_Pin);
	HAL_GPIO_WritePin(CHARGER_CE_GPIO_Port, CHARGER_CE_Pin, GPIO_PIN_RESET);
	{
		uint16_t in = 0;
		MAX_ReadRegister(MAX_CURRENT, &in);
		volatile float fcurrent = in * (1.5625f / 0.01f) / 10000.0f;
		batteryStatus.current = lroundf(fcurrent);
	}
	{
		uint16_t in = 0;
		MAX_ReadRegister(MAX_AVG_CURRENT, &in);
		volatile float fcurrent = in * (1.5625f / 0.01f) / 10000.0f;
		batteryStatus.avgCurrent = lroundf(fcurrent);
	}
	{
		uint16_t in = 0;
		MAX_ReadRegister(MAX_VCELL, &in);
		volatile float f = (in >> 3) * 0.625;
		batteryStatus.voltage = lroundf(f);
	}
	{
		uint16_t in = 0;
		MAX_ReadRegister(MAX_AVG_VCELL, &in);
		volatile float f = (in >> 3) * 0.625;
		batteryStatus.avgVoltage = lroundf(f);
	}

	uint16_t soc = 0;
	MAX_ReadRegister(MAX_SOC_MIX, &soc);
	volatile float fsoc = (soc >> 8) + (soc & 0xff) * 0.0039;
	batteryStatus.percentage = lround(fsoc);

	batteryStatus.status = GOOD;
	if (pgood) {
		batteryStatus.status = FULL;
	} else {
		if (chargeStatus) {
				batteryStatus.status = CHARGING;
		} else {
			if (fsoc < 30) {
				batteryStatus.status = LOW;
			} else {
				batteryStatus.status = GOOD;
			}
		}
	}
}

