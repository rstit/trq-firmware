#ifndef WIFI_STA_H_
#define WIFI_STA_H_

#include <wifi.h>


WiFi_Status_t WIFI_STA_Connect(const char* ssid, const char* password, WiFi_Priv_Mode privMode);
void WIFI_STA_Start();



#endif /* WIFI_STA_H_ */
