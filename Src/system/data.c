#include "structures.h"
#include "gpio.h"
#include "stm32f3xx.h"
#include "main.h"
#include "system/data.h"

#include <string.h>

AnalogMeasurementConfig analogConfig;
WiFiConfig wifiConfig;
BatteryData batteryData;

const FlashFragment Mem_fragments[] ={
		{&wifiConfig, sizeof(wifiConfig), 0x200, 0x49783498},
		{&batteryData, sizeof(batteryData), 0x800, 0x79887951},
		{&analogConfig, sizeof(analogConfig), 0x300, 0x95187645},
};

uint8_t Flash_Status;
uint8_t Flash_Config;

void data_Reload() {
	Flash_ResetEnable();
	volatile uint32_t jedecId = Flash_ReadJedec();
	if (jedecId == 0) {
		printf("[MEM] Flash communication error\r\n");
		return;
	}
	for (int i = 0; i < 2; i++) {
		const FlashFragment *fragment = &Mem_fragments[i];
		if (Flash_ReadBlock(fragment) == HAL_OK) {
			printf("[MEM] block %d restored\r\n", i);
		} else {
			printf("[MEM] Default %d settings\r\n", i);
			memset(fragment->ptr, 0, fragment->blockLength);
			//strcpy(wifiConfig.ssid, "AchillesArmour");
			//strcpy(wifiConfig.password, "");
			//wifiConfig.flags = 0;
			Flash_WriteBlock(fragment);
			HAL_Delay(20);
			Flash_ReadBlock(fragment);
		}
	}
}

void data_Store() {
	Flash_ResetEnable();
	volatile uint32_t jedecId = Flash_ReadJedec();
	if (jedecId == 0) {
		printf("[MEM] Flash communication error\r\n");
		return;
	}
	for (int i = 0; i < 2; i++) {
		const FlashFragment *fragment = &Mem_fragments[i];
		if (Flash_WriteBlock(fragment) == HAL_OK) {
			printf("[MEM] block %d stored\r\n", i);
		}
	}
}

HAL_StatusTypeDef data_SaveWiFiConfig(){
	volatile uint32_t jedecId = Flash_ReadJedec();
	if (jedecId == 0) {
		printf("[MEM] Flash communication error\r\n");
		return HAL_ERROR;
	}
	const FlashFragment *fragment = &Mem_fragments[0]; // WiFI configuration
	HAL_StatusTypeDef result = Flash_WriteBlock(fragment);
	if(result == HAL_OK){
		printf("[MEM] WiFI configuration saved\r\n");
		HAL_Delay(20);
	}else{
		printf("[MEM] WiFI configuration save ERROR\r\n");
	}
	return result;
}

HAL_StatusTypeDef data_ClearWiFiConfig(){
	volatile uint32_t jedecId = Flash_ReadJedec();
	if (jedecId == 0) {
		printf("[MEM] Flash communication error\r\n");
		return HAL_ERROR;
	}
	memset(&wifiConfig, 0, sizeof(wifiConfig));
	const FlashFragment *fragment = &Mem_fragments[0]; // WiFI configuration
	memset(&wifiConfig, 0, sizeof(wifiConfig));
	HAL_StatusTypeDef result = Flash_WriteBlock(fragment);
	if(result == HAL_OK){
		printf("[MEM] WiFI configuration cleared\r\n");
		HAL_Delay(20);
	}else{
		printf("[MEM] WiFI configuration clear ERROR\r\n");
	}
	return result;
}


