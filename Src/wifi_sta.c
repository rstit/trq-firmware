#include <wifi.h>
#include <wifi_sta.h>
#include <wifi_ap.h>
#include <structures.h>
#include <string.h>

#define WIFI_SCAN_BUFFER_LIST	15
wifi_scan net_scan[WIFI_SCAN_BUFFER_LIST];

extern char receivedCommandBuffer[512];

static wifi_state_t wifi_state;

extern WIFI_Callbacks_t WIFI_Callbacks;
WIFI_Callbacks_t STA;

extern HAL_StatusTypeDef data_ClearWiFiConfig();
extern HAL_StatusTypeDef data_SaveWiFiConfig();

void sta_ind_wifi_socket_data_received(int8_t callback_server_id, int8_t socket_id,
		uint8_t * data_ptr, uint32_t message_size, uint32_t chunk_size, WiFi_Socket_t socket_type);
void sta_ind_wifi_error(WiFi_Status_t error_code);
void sta_ind_wifi_connected(void);


static void WIFI_STA_InitCallbacks(){
	STA.error = sta_ind_wifi_error;
	STA.data_received = sta_ind_wifi_socket_data_received;
	STA.connected = sta_ind_wifi_connected;

	WIFI_Callbacks = STA;
}


WiFi_Status_t WIFI_CheckConnectionParams(const char* ssid, WiFi_Priv_Mode privMode){
	WiFi_Status_t status = WiFi_MODULE_SUCCESS;
	//Check if WiFi by given SSID exist & module support network encryption type
	int SSID_index = -1;
	status = wifi_network_scan(net_scan, WIFI_SCAN_BUFFER_LIST);
	if(status == WiFi_MODULE_SUCCESS)
	{
	  for (int i = 0; i < WIFI_SCAN_BUFFER_LIST; i++)
		{
			//printf("Network: %s %d %d %d\r\n", net_scan[i].ssid, net_scan[i].sec_type.wpa, net_scan[i].sec_type.wpa2, net_scan[i].sec_type.wps);
			if(strcmpi((const char *)net_scan[i].ssid, ssid) == 0)
			{
				printf("\r\n >>Network present...connecting to AP...\r\n");
				SSID_index = i;
				break;
			}
		}
		if (SSID_index < 0){
			printf("\r\nGiven SSID not found or no sup!\r\n");
			status = WiFi_SSID_ERROR;
			printf("\r\nAvailable SSIDs:\r\n");
			  for (int i = 0; i < WIFI_SCAN_BUFFER_LIST; i++) {
					printf("\t%s\r\n", (const char *)net_scan[i].ssid);
			  }
		} else if(net_scan[SSID_index].sec_type.wpa != WIFI_FALSE){
			printf("\r\nGiven SEC_TYPE is wrong!! %d %d %d\r\n", net_scan[SSID_index].sec_type.wps?1:0, net_scan[SSID_index].sec_type.wpa?1:0, net_scan[SSID_index].sec_type.wpa2?1:0);
			status = WiFi_SecKey_ERROR;
		}
		memset(net_scan, 0x00, sizeof(net_scan));
	} else {
		printf("\r\n >>Network scan failed\r\n");
	}

	return status;
}


WiFi_Status_t WIFI_STA_Connect(const char* ssid, const char* password, WiFi_Priv_Mode privMode){

	WiFi_Status_t status = WiFi_MODULE_SUCCESS;
	unsigned char wifiPasswordHex[33];
	char deviceHostname[32];
	const char* defaultDeviceName = "Achilles\0";

	WIFI_STA_InitCallbacks();

	// Check if network is available and WiFi encryption is match
	status = WIFI_CheckConnectionParams(ssid, privMode & WIFI_FLAGS_MASK_PRIV_MODE);

	if(status != WiFi_MODULE_SUCCESS){
		printf("STA init fail ");
		//TODO: Remove later!!
//		data_ClearWiFiConfig();
		WIFI_LED_BlinkFault(2, 200);
//		NVIC_SystemReset();
		HAL_Delay(1000);
		//return status;
	}

	//Generate Host name from MAC address or use default
	if (WIFI_GetAPName() == NULL) {
		WiFi_Status_t macStatus = WIFI_FillMACAddress();
		if (macStatus != WiFi_MODULE_SUCCESS) {
			printf("Can't get MAC address\r\n");
			strncpy(deviceHostname, defaultDeviceName,
					strlen(defaultDeviceName));
		} else {
			WiFiAPName_t apNameStatus = WIFI_GenerateAPName();
			if (apNameStatus != wifiAPNameGenerated) {
				printf("Can't generate AP name.\r\n");
				strncpy(deviceHostname, defaultDeviceName,
						strlen(defaultDeviceName));
			} else {
				strncpy(deviceHostname, WIFI_GetAPName(),
						strlen(WIFI_GetAPName()));
			}
		}
	} else {
		strncpy(deviceHostname, WIFI_GetAPName(), strlen(WIFI_GetAPName()));
	}
	printf("STA host name: %s", deviceHostname);

	status |= SET_SSID((char*)ssid);
	//status |= WIFI_ExecuteCommandWithArgString(AT_SET_SSID, ssid);

	if(privMode == WEP){
		//convert WiFiPassword to hex format (for WEP)
		memset(wifiPasswordHex, 0x00, sizeof(wifiPasswordHex));
		int i, j;
		for(i=0, j=0; i < strlen(password); i++, j+=2)
		{
			sprintf((char*)wifiPasswordHex+j,"%x", password[i]);
		}
		wifiPasswordHex[j]='\0';
		//printf("STA pass: %s\r\n", wifiAPPasswordHex);

		for(i = j; i < 32; i+=2){
			sprintf((char*)wifiPasswordHex+i,"%d%d", 0, 0);
		}
		status |= WIFI_SetConfigurationAddress("wifi_wep_keys[0]", (const char*)wifiPasswordHex);
		status |= WIFI_SetConfigurationValue("wifi_wep_key_lens", strlen(password));
		status |= WIFI_SetConfigurationValue("wifi_wep_default_key", 0);
		status |= WIFI_SetConfigurationValue(WIFI_AUTH_TYPE, 0); // Authentication type used in STA, IBSS and MiniAP mode: 0=OpenSystem, 1=SharedKey

	}else if(privMode == WPA_Personal){
		status |= WIFI_ExecuteCommandWithArgString(AT_SET_SEC_KEY, password);
		status |= WIFI_SetConfigurationValue(WIFI_AUTH_TYPE, 0); // Authentication type used in STA, IBSS and MiniAP mode: 0=OpenSystem, 1=SharedKey
	}

	status |= WIFI_SetConfigurationValue(WIFI_PRIV_MODE, privMode);

	status |= WIFI_SetConfigurationAddress(WIFI_IP_HOSTNAME, deviceHostname);
	status |= WIFI_PreSetup();

	/*
		Radio Mode[default 1]:
		0 = IDLE
		1 = STA (Supported Security Modes: OPEN, WEP OpenSystem, WEP SharedKey, WPA/WPA2 - wifi_auth_type must be set to 0)
		2 = IBSS (Supported Security Modes: OPEN, WEP OpenSystem, WEP SharedKey)
		3 = MiniAP (Supported Security Modes:OPEN, WEP OpenSystem - Supported Classes: b,g) *
	 */
	status |= WIFI_SetConfigurationValue(WIFI_MODE, WiFi_STA_MODE);

	status |= WIFI_ExecuteCommand(AT_SAVE_CURRENT_SETTING);
	//status |= WIFI_ExecuteCommand(AT_RESET);
	wifi_reset();

	return status;

}

void WIFI_STA_Start(){
	WIFI_LED_OnOff(1);

	wifi_state = wifi_state_idle;//wifi_state_connected;
	HAL_Delay(3000);
	WiFi_Status_t status;

	//WIFI_GetAllConfiguration();

	while (1){
		 switch (wifi_state)
		 {
		   case wifi_state_reset:
		   break;
		   case wifi_state_ready:
			   printf("\r\n >>STA ready.\r\n");
			   HAL_Delay(200);
			   fflush(stdout);
			  wifi_state = wifi_state_idle;
		   break;
		   case wifi_state_connected:
			printf("\r\n>>STA Connected\r\n");
			wifi_state = wifi_state_socket;
			break;
		   case wifi_state_disconnected:
			   printf("\r\n >>disconnected..\r\n");
			   //wifi_state = wifi_state_idle;
			   wifi_state = wifi_state_reset;
			break;
		   case wifi_state_socket:
			   printf("\r\n>>WiFi server socket opening..\r\n");
			   while(1) {
				   status = wifi_socket_server_open(WIFI_TCPServer_PortNumber, (uint8_t *)WIFI_TCPServer_Protocol);
				   if(status == WiFi_MODULE_SUCCESS)
					{
					   printf(">>Server Socket Open OK \r\n");
					   break;
					}else{
						 printf(">>Server Socket Open FAIL %d\r\n", status);
						 HAL_Delay(100);
					}
			   }
				wifi_state = wifi_state_idle;
			 break;
		   case wifi_state_socket_write:
			   printf("\r\n >>Writing data to client \r\n");
			   wifi_state = wifi_state_idle;
			 break;
		   case wifi_state_idle:
			   /* For debug
			   idleCount++;
			   if(idleCount >= 100){
				printf(".");
				fflush(stdout);
				idleCount = 1;
			   }
			   */
			   wifiIdleTask();
			 break;
		   default:
			   break;
		 }
	}
}




void sta_ind_wifi_connected()
{
  printf("\r\nSTA ind_wifi_connected()\r\n");
  wifi_state = wifi_state_connected;
}

void sta_ind_wifi_socket_data_received(int8_t callback_server_id, int8_t socket_id,
		uint8_t * data_ptr, uint32_t message_size, uint32_t chunk_size, WiFi_Socket_t socket_type)
{
	//printf("STA nData Receive Callback...\r\n");
	//printf("Socket ID: %d ServerId: %d type: %d\r\n",socket_id, callback_server_id, socket_type);
	memcpy(receivedCommandBuffer, data_ptr, 512);
	COM_FromWifi(receivedCommandBuffer);
	wifi_state = wifi_state_idle;		// Do not REMOVE !!!!!!!
}

void sta_ind_wifi_error(WiFi_Status_t error_code)
{
  printf("\r\nSTA ind_wifi_error %d 0x%x\r\n", error_code, error_code);
  if(error_code == WiFi_AT_CMD_RESP_ERROR)
  {
    //wifi_state = wifi_state_idle;
    printf("\r\n WiFi Command Failed. \r\n User should now press the RESET Button(B2). \r\n");
  }
}






