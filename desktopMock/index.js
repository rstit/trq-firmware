const fs = require('fs')
const net = require('net')
const Proto = require('./protocol')
const _ = require('lodash')

const recordFile = __dirname + '/record/' + new Date().toISOString().replace(/[^a-zA-Z0-9]/g, '_') + '.txt';

var proto = new Proto();

const readline = require('readline');
const colors = require('colors') // npm install colors
//readline.emitKeypressEvents(process.stdin);
//process.stdin.setRawMode(true);

const keyMap = new Map();
keyMap.set('b', 'Send "SysCall: beep" frame');
keyMap.set('p', 'Send "GetStruct: Power" frame');
keyMap.set('l', 'Send "SysCall: beep LED" frame');
keyMap.set('r', 'Send "SysCall: reset" frame');
keyMap.set('c', 'Send "Connect to STA" frame');
keyMap.set('t', 'Send "Test" frames');
keyMap.set('f', 'Set frequency');
//keyMap.set('h', 'Send "Handshake" frame');
keyMap.set('u', 'Send "Unknown" frame');

const config = {
  port: 3200,
  // ip: '172.30.38.1'
  ip: '172.30.51.1'
  //ip: '192.168.43.97'
}

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var client = new net.Socket();

var recordFrame = function (arrow, data) {
  fs.appendFile(recordFile, "frame =>: " + data[0].toString(16) + ": " + data.toString("hex") + "\n");
}

var _write = client.write;

// client.setTimeout(3000);
client.on('timeout', () => {
  console.log('socket timeout');
  client.destroy();
});

client.connect(config.port, config.ip, function () {
  console.log('Connected');
});

client.on('error', (error) => {
  console.log('Error: ' + error.stack);
  console.log();
});

client.on('close', function () {
  console.log('Connection closed');
});

let lastValidFrame = new Date();
client.on('data', function (data) {
  try {
    console.log('raw: ', data)
    //recordFrame("<=", data)

    let frame = new Proto().read(data).Frames().result
    // console.log('Received frame: ', JSON.stringify(frame, null, 2))
    // console.log('since last: ', new Date() - lastValidFrame)
    lastValidFrame = new Date()
  } catch (e) {
    //console.error(e)
    switch (data[0]) {
      case 0x81: // respnse for FRAME_ReadDataResponse
        console.log('Response for \'ReadDataResponse\' received: ' + data.toString('hex'));
        break;
      case 0x82: // response for FRAME_WriteDataResponse
        console.log('Response for \'WriteDataResponse\' received: ' + data.toString('hex'));
        break;
      case 0x10:
        try {
          let frame = new Proto().read(data).MeasurementFrame().result;
          //console.log('Received frame: ', JSON.stringify(frame));
          console.log('since last: ', new Date() - lastValidFrame);
          lastValidFrame = new Date()
        } catch (e) {
          console.log('Catch MeasurementFrame Received: ' + data.toString('hex'));
          // console.error(e)
        }
        break;
      case 0x84: // response for FRAME_SysCallResponse
        try {
          let frameS = new Proto().read(data).SysCallFrame().result;
          console.log('SysCall Received: ', JSON.stringify(frameS));
        } catch (e) {
          console.log('Catch SysCall received: ' + data.toString('hex'));
        }
        break;
      case 0x85: //FRAME_TestFrameResponse

        testFrameReceiveTime = new Date();
        testFrameSendNext = true;
        try {
          //console.log('Test raw frame: ' +  data.toString('hex'))
          let frame = new Proto().read(data).TestFrame().result;
          console.log('Test frame received: ', (testFrameReceiveTime - testFrameSendTime));
          //console.log('Received Test frame: ', JSON.stringify(frame))
        } catch (e) {
          //console.log('Test frame received: ERROR');
        }
        console.log('Test frame received  end: ', (testFrameReceiveTime - testFrameSendTime));
        //console.log('Test frame received: ' + data[0] + ' time: ', (testFrameReceiveTime - testFrameSendTime));

        break;

      default:
        console.log('Not supported response frame received: ' + data.toString('hex'));
        break;
    }
  }
});

function listKeys() {
  //console.log(`${eol}keys`);
  keyMap.forEach((value, key) => {
    console.log(`${key} - ${value}`);
  });
  console.log();
}

rl.on('SIGINT', function (cmd) {
  process.exit(0);
});

process.stdin.on('keypress', (str, key) => {
  if (key.ctrl) {
    if (key.name === 'c') {
      //Fxit from app
      process.exit();
    } else {
      console.log(`You pressed the Ctrl+"${key.name}" key`);
    }
  } else {
    console.log(`You pressed the "${key.name}" key`);
    //console.log(key);

    switch (key.name) {
      case 'c':
        frameC = createWiFiConfigFrame("H3235", "1234567890123", 0x0002);
        //recordFrame('=>', frameC);
        client.write(frameC);
        break;
      case 'b':
        testFrameSendNext = true;
        sendBeepFramesTest();

        var frameL = createSysCallFrame(0x01);
        recordFrame('=>', frameL);
        client.write(frameL);
        break;
      case 'l':
        var frameB = createSysCallFrame(0x02);
        recordFrame('=>', frameB);
        client.write(frameB);
        break;
      case 'r':
        var frameR = createSysCallFrame(0x03);
        recordFrame('=>', frameR);
        client.write(frameR);
        break;
      case 'u':
        var frameU = createUnknownFrame();
        recordFrame('=>', frameU);
        client.write(frameU);
        break;
      case 'p':
        let buffer = new Proto().write().ReadData({
          structId: 0x02
        }).result
        client.write(buffer)
        break
      case 'f':
        let setFrame = new Proto().write().WriteData({
          structId: 0x03,
          data: {
            frequency: _.sample([1000, 2000, 5000, 10000]),
            attenuator: 1
          }
        })
        console.log('write ', setFrame.result)
        client.write(setFrame.result)
        break

      case 't': // send test frames
        testFrameSendNext = true;
        sendTestFrames(700);
        break;
    }
  }
});

const snooze = ms => new Promise(resolve => setTimeout(resolve, ms));

var testFrameSendTime = new Date();
var testFrameReceiveTime = new Date();
var testFrameSendNext = false;

const sendTestFrames = async (frameResponseDataSize) => {
  var numberTestFramesToSend = 25;

  var avgTime = 0;
  console.log('Send ' + numberTestFramesToSend + ' frames, eatch response with data size: ' + frameResponseDataSize + ' ...');
  for (var i = 0; i < numberTestFramesToSend; i++) {

    //var frame = new Proto().write(0x20, 1, numberTestFramesToSend).result;
    var frame = createTestFrame(1, frameResponseDataSize);

    while (testFrameSendNext != true) {
      //console.log('while');
      await snooze(800);
    }
    testFrameSendTime = new Date(); // start time measure
    testFrameSendNext = false;
    //console.log('Sending next...');
    client.write(frame, function () {
      //console.log('Callback');
    });
  }

  console.log("End");
};

const sendBeepFramesTest = async () => {
  var count = 25;
  var avgTime = 0;
  //console.log('About to snooze without halting the event loop...');
  for (var i = 0; i < count; i++) {
    var frame = createSysCallFrame(0x01);

    while (testFrameSendNext != true) {
      //console.log('while');
      await snooze(800);
    }
    testFrameSendTime = new Date(); // start time measure
    testFrameSendNext = false;
    //console.log('Sending next...');
    client.write(frame, function () {
      //console.log('Callback');
    });
  }

  console.log("End");

  //console.log('done!');
};

/**
 * Generates config frame
 * @param  {String} SSID     
 * @param  {String} password 
 * @param  {Number} privMode: 0x0000 - no encription, 0x0001 - wep128 (not teted),
 *                            0x0002 - wpa/wpa2
 * @return {[type]}          [description]
 */
function createWiFiConfigFrame(SSID, password, privMode) {
  var maxSSIDLength = 32;
  var maxPASSWORDLength = 32;

  // TODO - it should probably be provMode == 1
  if (privMode == 2 && password.length != 13) {
    console.log("WEP-128 support only 13-characters passwords.");
    return;
  }

  var structSize = maxSSIDLength + maxPASSWORDLength + 2;
  var ssidArray = getBytes(SSID);
  var passwordArray = getBytes(password);

  var fillSSIDArray = Array(maxSSIDLength - SSID.length);
  fillSSIDArray.fill(0x00);
  Array.prototype.push.apply(ssidArray, fillSSIDArray);

  var fillPasswordArray = Array(maxPASSWORDLength - password.length);
  fillPasswordArray.fill(0x00);
  Array.prototype.push.apply(passwordArray, fillPasswordArray);

  var buffer = proto
    .write()
    .UInt8(0x02) // 0x02 - WriteFrame
    .UInt16LE(1 + 2 + 1 + 4 + structSize) // whole frame size
    .UInt8(0x0f) // 0x0F - WiFiConfigStruct
    .UInt32LE(structSize) // WiFiConfigStruct size
    .raw(ssidArray)
    .raw(passwordArray)
    .UInt16LE(0xFFFF & privMode)
    .result;

  return buffer;
};

function createSysCallFrame(functionValue) {
  //functionValue: 1 - Beep, 2 - Beep LEDs, 3 - Reset
  var buffer = proto
    .write()
    .UInt8(0x04) // 0x04 - SysCall frame
    .UInt16LE(1 + 2 + 1) // whole frame size
    .UInt8(0xff & functionValue)
    .result;
  return buffer;
};

function createTestFrame(frameDataSize, frameResponseDataSize) {
  var testData = Array(frameDataSize);
  testData.fill(0xff);
  var buffer = proto
    .write()
    .UInt8(0x05) // 0x05 - FRAME_TestFrame 
    .UInt16LE(1 + 2 + 2 + frameDataSize) // frame size
    .UInt16LE(frameResponseDataSize)
    .raw(testData)
    .result;
  return buffer;
};


function createUnknownFrame() {
  var buffer = proto
    .write()
    .UInt8(randomInt(0x21, 0x7F)) // Random ID
    .UInt16LE(randomInt(0, 0xFFFF)) // whole frame size, random
    .result;
  return buffer;
};

function getBytes(str) {
  var bytes = [], char;
  str = encodeURI(str);

  while (str.length) {
    char = str.slice(0, 1);
    str = str.slice(1);

    bytes.push(char.charCodeAt(0));
  }
  return bytes;
};

function randomInt(low, high) {
  return Math.floor(Math.random() * (high - low) + low);
};

//////////////////////////////////////////////////////////////////////////////////////////////
console.log('Press a key to send frame:');
listKeys();

/*
rl.question('Select command: ', (command) => {
  exec(command.trim());
  rl.close();
});

function exec(command) {
  switch(command){
    case 'c':
      var ssidNetwork = '';
      var passNetwork = '';

      rl.question("Enter SSID network [H32]: ", function(answer) {
        //if(answer == null){
        //  ssidNetwork = 'H32';
        //}else{
          ssidNetwork = answer;
        //}
        console.log(`Thank you for your valuable feedback: ${answer}`);
       // rl.close();
      });

      rl.question('Enter PASSWORD network [1234567890123]: ', function(answer2){
        if(answer2 == null){
          passNetwork = "1234567890123";
        }else{
          passNetwork = answer2;
        }
        //console.log(`Thank you for your valuable feedback: ${answer}`);
       // rl.close();
      });

      console.log(">SSID:");
      console.log(ssidNetwork);
      console.log(">PASS:");
      console.log(passNetwork);
      //var frameC = createWiFiConfigFrame("RST_AP", "RST@RR5SvYFdC7Ys!", 0x0002);
      //frameC = createWiFiConfigFrame("H32", "1234567890123", 0x0001);
      //client.write(frameC);
    break;
    case 'b':
      var frameB = createSysCallFrame(0x01);
      client.write(frameB);
    break;
    case 'r':
      var frameR = createSysCallFrame(0x02);
      client.write(frameR);
    break;

  }
};
*/