/*
 * utils.h
 *
 *  Created on: 07.05.2018
 *      Author: jkonieczny
 */

#ifndef UTILS_H_
#define UTILS_H_

#ifdef __cplusplus
extern "C" {
#endif

#define For(a, n) for (int a = 0; a < n; a++)
#define each(item, array, length) \
		(typeof(*(array)) *p = (array), (item) = *p; p < &((array)[length]); p++, (item) = *p)

#ifdef __cplusplus
}
#endif

#endif /* UTILS_H_ */
