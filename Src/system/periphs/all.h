/*
 * all.h
 *
 *  Created on: 11.05.2018
 *      Author: jkonieczny
 */

#ifndef PERIPHS_ALL_H_
#define PERIPHS_ALL_H_

#include <stdint.h>
#include "stm32f3xx_hal.h"
#include "structures.h"

extern UART_HandleTypeDef huart1;

/**
 * state of device
 * INIT goes to CONF when all on-chip peripherals are configured
 * CONF loads configurations and connects to WiFi
 * IDLE waits for pressing Scan button
 * SCAN performs measurements waiting for releasing Scan button
 */
typedef enum {
	PREINIT = 0,
	INIT = 1,
	CONF = 2,
	IDLE = 3,
	SCAN = 4,
	SLEEP = 10
} DeviceState;

typedef struct DeviceStatus {
	uint32_t scanButtonPressed;
	uint32_t powerButtonPressed;
	uint32_t powerTimeout;
	DeviceState state;
	uint32_t requestRestart;
} DeviceStatus;

extern DeviceStatus deviceStatus;

#define QUEUE_LENGTH 32

typedef struct FrameToSend {
	Frame *frame;
	int size;
	uint32_t timestamp;
	COM_Tx tx;
} FrameToSend;

extern FrameToSend framesToSend[QUEUE_LENGTH];

FrameToSend frames_GetReadyFrame();

typedef struct PRegister {
	uint8_t address;
	uint8_t length;
} __attribute__((packed)) PRegister;

// DDS interface
static const PRegister DDS_CFR1 = {0x00, 4};
static const PRegister DDS_CFR2 = {0x01, 2};
static const PRegister DDS_DAC = {0x02, 4};
static const PRegister DDS_FTW = {0x03, 4};
static const PRegister DDS_POW = {0x04, 2};
static const PRegister DDS_LSP = {0x06, 8};
static const PRegister DDS_LSD = {0x07, 8};
static const PRegister DDS_LSR = {0x08, 4};

HAL_StatusTypeDef DDS_WriteRegister(PRegister reg, void* data);
HAL_StatusTypeDef DDS_ReadRegister(PRegister reg, void* data);

// Attenuator interface
HAL_StatusTypeDef ATT_Set(uint8_t value);


void updateBatteryState();
//Cell fuel gauge
struct MaxStatusRegister {
	uint16_t _0:1;
	uint16_t PoweronReset:1;
	uint16_t _2:1;
	uint16_t BatteryStatus:1;
	uint16_t _4:1;
	uint16_t _5:1;
	uint16_t _6:1;
	uint16_t _7:1;

	uint16_t Vmn:1;
	uint16_t Tmn:1;
	uint16_t Smn:1;
	uint16_t Bi:1;
	uint16_t Vmx:1;
	uint16_t Tmx:1;
	uint16_t Smx:1;
	uint16_t Br:1;
} __attribute__((packed));

union MaxConfigRegister {
	uint16_t raw;
	struct {
		uint16_t Ber:1;
		uint16_t Bei:1;
		uint16_t Aen:1;
		uint16_t FTHRM:1;
		uint16_t ETHRM:1;
		uint16_t ALSH:1;
		uint16_t I2CSH:1;
		uint16_t SHDN:1;

		uint16_t Tex:1;
		uint16_t Ten:1;
		uint16_t AINSH:1;
		uint16_t ALRTp:1;
		uint16_t Vs:1;
		uint16_t Ts:1;
		uint16_t Ss:1;
		uint16_t _0:1;
	}__attribute__((packed));
} ;
static const PRegister MAX_STATUS = {0x00, 2};
static const PRegister MAX_CONFIG = {0x1D, 2};

static const PRegister MAX_TEMPERATURE = {0x08, 2};
static const PRegister MAX_VCELL= {0x09, 2};
static const PRegister MAX_CURRENT= {0x0A, 2};
static const PRegister MAX_AVG_CURRENT= {0x0B, 2};
static const PRegister MAX_AVG_VCELL = {0x19, 2};


static const PRegister MAX_SOC_MIX = {0x0d, 2};
static const PRegister MAX_SOC_AV = {0x0e, 2};
static const PRegister MAX_SOC_REP = {0x06, 2};
static const PRegister MAX_REM_CAP = {0x0f, 2};

static const PRegister MAX_FULL_SOC_THR = {0x13, 2};

static const PRegister MAX_AGE = {0x07, 2};
static const PRegister MAX_AIN = {0x27, 2};
static const PRegister MAX_FILTER_CFG = {0x29, 2};

static const PRegister MAX_VFOCV = {0xFB, 2};

static const PRegister MAX_V_EMPTY = {0x3A, 2};

static const PRegister MAX_TGAIN = {0x2C, 2};
static const PRegister MAX_TOFF = {0x2D, 2};
static const PRegister MAX_CGAIN = {0x2E, 2};
static const PRegister MAX_COFF = {0x2F, 2};

static const PRegister MAX_DESIGN_CAP = {0x18, 2};
static const PRegister MAX_FULL_CAP_NOM = {0x32, 2};
// Save&Restore
static const PRegister MAX_FULL_CAP = {0x10, 2};
static const PRegister MAX_CYCLES = {0x17, 2};
static const PRegister MAX_RCOMP0 = {0x38, 2};
static const PRegister MAX_TEMP_CO = {0x39, 2};
static const PRegister MAX_QRES_00 = {0x12, 2};
static const PRegister MAX_QRES_10 = {0x22, 2};
static const PRegister MAX_QRES_20 = {0x32, 2};
static const PRegister MAX_QRES_30 = {0x42, 2};
static const PRegister MAX_QACC = {0x45, 2};
static const PRegister MAX_PACC = {0x46, 2};

static const PRegister* MAX_STORE[] = {
		&MAX_FULL_CAP,
		&MAX_CYCLES,
		&MAX_RCOMP0,
		&MAX_TEMP_CO,
		&MAX_QRES_00,
		&MAX_QRES_10,
		&MAX_QRES_20,
		&MAX_QRES_30,
		&MAX_QACC,
		&MAX_PACC
};



// Accelerometer interface
static const PRegister ACC_STATUS = {0x00, 1};
static const PRegister ACC_SYSMODE = {0x0B, 1};
static const PRegister ACC_INT_SOURCE = {0x0C, 1};
static const PRegister WHO_AM_I = {0x0D, 1};
static const PRegister ACC_XYZ_CFG = {0x0E, 1};
static const PRegister ACC_FIFO = {0x09, 1};
static const PRegister ACC_PL = {0x11, 1};
static const PRegister ACC_MT_CFG = {0x15, 1};
static const PRegister ACC_EA = {0x16, 1};
static const PRegister ACC_MT_SRC = {0x16, 1};
static const PRegister ACC_MT_THS = {0x17, 1};
static const PRegister ACC_MT_COUNT = {0x18, 1};
static const PRegister ACC_TRANSIENT = {0x1E, 1};
// FF_MT_SRC
static const PRegister ACC_CTRL_REG = {0x2A, 5};
static const PRegister ACC_CTRL_REG1 = {0x2A, 1};
static const PRegister ACC_CTRL_REG2 = {0x2B, 1};
static const PRegister ACC_CTRL_REG3 = {0x2C, 1};
static const PRegister ACC_CTRL_REG4 = {0x2D, 1};
static const PRegister ACC_CTRL_REG5 = {0x2E, 1};
// Helping structs

typedef struct ACC_Status {
	uint8_t cnt:6;
	uint8_t wmrk:1;
	uint8_t ovf:1;
} __attribute__((packed)) ACC_Status;

typedef struct ACC_IntSrc {
	uint8_t drdy:1;
	uint8_t ___:1;
	uint8_t ff_mt:1;
	uint8_t pulse:1;
	uint8_t lndprt:1;
	uint8_t trans:1;
	uint8_t fifo:1;
	uint8_t aslp:1;
} __attribute__((packed)) ACC_IntSrc;


// Functions
HAL_StatusTypeDef ACC_Init();
HAL_StatusTypeDef ACC_Start();
HAL_StatusTypeDef ACC_Stop();


HAL_StatusTypeDef ADC_Init();
HAL_StatusTypeDef ADC_Start();
HAL_StatusTypeDef ADC_Stop();


//handlers
void ACC_IrqHandler();

#endif /* PERIPHS_ALL_H_ */
