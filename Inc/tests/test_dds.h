#ifndef _TEST_DDS_H
#define _TEST_DDS_H


void DDS_test_PowerUp();
void DDS_test_PowerDown();

void DDS_test_10Hz();
void DDS_test_100Hz();
void DDS_test_1kHz();
void DDS_test_10kHz();
void DDS_test_100kHz();
void DDS_test_1MHz();


#endif
