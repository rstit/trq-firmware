
#include "stm32f3xx.h"
#include "stm32f3xx_hal.h"
#include "usart.h"

#include "wifi_module.h"
#include "wifi_globals.h"
#include "wifi_interface.h"
#include "wifi.h"
#include "system/data.h"

#include "wifi_ap.h"
#include "structures.h"
void power_KeepAlive();
// WIFI methods
WIFI_Result_t WIFI_Main();
void WIFI_PowerUp();
void WIFI_Reset();
void WIFI_RestoreDefaultSettingsByPinState();
void WIFI_InitState();
WiFi_Status_t WIFI_AP_Start();

// Configuration saved in FLASH
extern WiFiConfig wifiConfig;

//wifi_config config;

inline
void WIFI_SetAPMode(){
	HAL_GPIO_WritePin(WLAN_MODE_GPIO_Port, WLAN_MODE_Pin, GPIO_PIN_RESET);
}

inline
void WIFI_SetSTAMode(){
	HAL_GPIO_WritePin(WLAN_MODE_GPIO_Port, WLAN_MODE_Pin, GPIO_PIN_SET);
}

WIFI_Result_t WIFI_Main(){

	WIFI_Result_t result = WIFI_SUCCESS;
	WiFi_Status_t status = WiFi_MODULE_SUCCESS;

	//WIFI_RestoreDefaultSettingsByPinState();
	//data_ClearWiFiConfig();

	data_Reload();
	power_KeepAlive();

	//Read configuration from Flash
	// If has WiFi config, then init module with STA configuration
	WiFi_Mode_TypeDef wifiWorkMode = WiFi_MiniAP_MODE;

	//if(wifiWorkMode != WiFi_MiniAP_MODE){
	if(wifiConfig.ssid[0] != 0){ // Configuration is saved in FLASH
#ifdef DEBUG_PRINT
		printf("\r\nOn Start FLASH saved settings: %s %s %d\r\n", wifiConfig.ssid, wifiConfig.password, wifiConfig.flags);
#endif
		wifiWorkMode = WiFi_STA_MODE; // default mode: AP
		WIFI_SetSTAMode();
	}else{
		wifiWorkMode = WiFi_MiniAP_MODE;
		WIFI_SetAPMode();
	}

	power_KeepAlive();
	WIFI_PowerUp();
	WIFI_Reset();
	power_KeepAlive();

	Timer_Config();
	WIFI_InitState();
	UART_Configuration(WiFi_USART_BAUD_RATE);

	power_KeepAlive();

//	WIFI_ExecuteCommand("AT+S.HELP\r");
//	HAL_Delay(2000);

#ifdef DEBUG_PRINT
	printf("\r\n\nInitializing the wifi module...");
#endif
	/* Init the wi-fi module */
	power_KeepAlive();
	status = WIFI_Module_Init(WiFi_USART_BAUD_RATE);
	//status = wifi_init(&config);
	if(status != WiFi_MODULE_SUCCESS)
	{
#ifdef DEBUG_PRINT
		printf("Error in WiFi configuration");
		WIFI_PrintStatus(status);
#endif
		return WIFI_CONFIG_ERR;
	}

	power_KeepAlive();
	if(wifiWorkMode == WiFi_MiniAP_MODE){
#ifdef DEBUG_PRINT
		printf("\r\nWIFI AP start...");
#endif
		status = WIFI_AP_Start();
		if(status != WiFi_MODULE_SUCCESS){
#ifdef DEBUG_PRINT
			printf("\r\nError in WiFi AP start\r\n");
			WIFI_PrintStatus(status);
#endif
			result = WIFI_START_ERR;
		}
	}else{
#ifdef DEBUG_PRINT
		printf("\r\nTry to connect as STA...");
#endif
		// Proper init sequence to connect to STA:
		status = WIFI_STA_Connect(wifiConfig.ssid, wifiConfig.password, (wifiConfig.flags & WIFI_FLAGS_MASK_PRIV_MODE));
		if(status != WiFi_MODULE_SUCCESS){
#ifdef DEBUG_PRINT
			printf("\r\nError in WiFi STA start\r\n");
			WIFI_PrintStatus(status);
#endif
			result = WIFI_START_ERR;
		}else{
			data_SaveWiFiConfig();
			WIFI_STA_Start();
		}
#ifdef DEBUG_PRINT
		printf("STA configuration result : %d\r\n", status);
#endif
	}

	return result;


}



