/*
 * measurements.c
 *
 *  Created on: 07.05.2018
 *      Author: jkonieczny
 *
 * Measurement and it's buffers management
 */

#include "structures.h"
#include "measurements.h"
#include "utils.h"
#include "periphs/all.h"

#define ADC_BUFFERS 20
#define ACC_BUFFERS 20

MeasurementFrame adcBuffer[ADC_BUFFERS];
MeasurementFrame accBuffer[ACC_BUFFERS];

volatile MeasurementFrame *currentAdcBuffer;
volatile MeasurementFrame *currentAccBuffer;
FrameToSend framesToSend[QUEUE_LENGTH];
DeviceConfig deviceConfig;

enum {
	M_ADC = 0x01, M_ACC = 0x02
};

#define QUEUE_MEAS_LENGTH 10
#define QUEUE_ITEM_SIZE sizeof(void*)

uint8_t measurementQueueBuffer[QUEUE_MEAS_LENGTH * sizeof(void*)];

void buffers_init() {

}

void buffers_resetAdcBuffer(int i) {
	adcBuffer[i].function = 0x10;
	adcBuffer[i].frameSize = 0;
	adcBuffer[i].delta = 0;
	adcBuffer[i].count = 0;
	adcBuffer[i].type = M_ADC;
}

void buffers_resetAccBuffer(int i) {
	accBuffer[i].function = 0x10;
	accBuffer[i].frameSize = 0;
	accBuffer[i].delta = 0;
	accBuffer[i].count = 0;
	accBuffer[i].type = M_ACC;
}

void buffers_finalizeBuffer(MeasurementFrame *buffer, int items) {
	if (buffer == 0) {
		return;
	}
	buffer->function = 0x10;
	buffer->count = items;
	currentAdcBuffer->delta = 100;
	buffer->frameSize = sizeof(MeasurementFrameHeader) + items * 2 * 3;
}

void buffers_clear () {
	for (int i = 0; i<ACC_BUFFERS; i++) {
		memset(&accBuffer[i], 0, sizeof(MeasurementFrame));
	}
	for (int i = 0; i<ADC_BUFFERS; i++) {
		memset(&adcBuffer[i], 0, sizeof(MeasurementFrame));
	}
}

void buffers_nextAdcBuffer() {
	for (int i = 0; i < ADC_BUFFERS; i++) {
		if (adcBuffer[i].timestamp == 0) {
			currentAdcBuffer = &adcBuffer[i];
			currentAdcBuffer->function = 0x10;
			currentAdcBuffer->type = M_ADC;
			currentAdcBuffer->timestamp = HAL_GetTick();
			return;
		}
	}
	currentAdcBuffer = 0;
}

void buffers_nextAccBuffer() {
	for (int i = 0; i < ACC_BUFFERS; i++) {
		if (accBuffer[i].timestamp == 0) {
			currentAccBuffer = &accBuffer[i];
			currentAccBuffer->function = 0x10;
			currentAccBuffer->type = M_ACC;
			currentAccBuffer->timestamp = HAL_GetTick();
			return;
		}
	}
	currentAccBuffer = 0;
}

/**
 * @brief Sends request to send frame to desktop app
 */
void frame_send(Frame *frame, int size) {
	__disable_irq();
	for (int i = 0; i < QUEUE_MEAS_LENGTH; i++) {
		if (framesToSend[i].frame == 0) {
			framesToSend[i].frame = frame;
			framesToSend[i].size = size;
			framesToSend[i].timestamp = HAL_GetTick();
//			framesToSend[i].tx = WiFi_response;
			break;
		}
	}
	__enable_irq();
}

/**
 * @brief returns most important buffer to send
 * @return pointer to buffer to send or {0, 0} if there is none
 */
FrameToSend frames_GetReadyFrame() {
	uint32_t ts = HAL_GetTick() + 1;

	volatile int index = -1;
	for (int i = 0; i < QUEUE_MEAS_LENGTH; i++) {
		Frame *frame = framesToSend[i].frame;
		if (frame) {
			index = i;
			break;
		}
	}
	if (index != -1) {
		FrameToSend ret = framesToSend[index];
		framesToSend[index].frame = 0;
		return ret;
	}
	FrameToSend ret = { 0, 0, 0, 0 };
	return ret;
}

/**
 * @brief Execute before _main, reset all buffers
 */
void
__attribute__((constructor (101)))
ctr__measurement_init() {
	currentAdcBuffer = 0;
	currentAccBuffer = 0;
	for (int i = 0; i < ADC_BUFFERS; i++) {
		buffers_resetAdcBuffer(i);
	}
	for (int i = 0; i < ACC_BUFFERS; i++) {
		buffers_resetAccBuffer(i);
	}
}
