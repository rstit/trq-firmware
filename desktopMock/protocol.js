const Proto = require('bin-protocol')


const frameType = {}
const structs = {}

Proto.define('MeasureStamp', {
  read: function () {
    this.UInt16LE('x')
    this.UInt16LE('y')
    this.UInt16LE('z')
  },
  write (data) {
    if (Array.isArray(data)) {
      for (let i = 0; i < 3; i++) {
        this.UInt16LE(data[i])
      }
    } else {
      this.UInt16LE(data.x)
      this.UInt16LE(data.y)
      this.UInt16LE(data.z)
    }
  }
})

Proto.define('String', {
  read: function (_, arg1, arg2) {
    let arr = []
    const length = 32
    while (arr.length < length) {
      this.UInt8('char')
      if (this.context.char == 0) {
        break;
      }
      arr.push(String.fromCharCode(this.context.char))
    }
    let left = length - arr.length - 1
    while (left--) {
      this.UInt8('_')
    }
    return arr.join('')
  },
  write({
    str,
    length
  }) {
    this.loop(Array.from(str).map(i => i.charCodeAt(0)), this.UInt8, str.length)
    let fill = length - str.length
    while (fill--) this.UInt8(0)
  }
})


structs[0x01] = 'StructWifiConfig'
Proto.define('StructWifiConfig', {
  read() {
    this.String('ssid', 32)
    this.String('password', 32)
    this.UInt16LE('flags')
  },
  write({
    ssid,
    password,
    flags
  }) {
    this.String({
      str: ssid,
      length: 32
    })
    this.String({
      str: password,
      length: 32
    })
    this.UInt16LE(flags)
  }
})


structs[0x02] = 'StructBatteryStatus'
Proto.define('StructBatteryStatus', {
  write(data) {
    this.UInt16LE(data['voltage'])
    this.UInt16LE(data['avgVoltage'])
    this.UInt16LE(data['current'])
    this.UInt16LE(data['avgCurrent'])
    this.UInt8(data['percentage'])
    this.UInt8(data['status'])
  },
  read() {
    this.UInt16LE('voltage')
    this.UInt16LE('avgVoltage')
    this.UInt16LE('current')
    this.UInt16LE('avgCurrent')
    this.UInt8('percentage')
    this.UInt8('status')
  }
})

structs[0x03] = 'StructAnalogData'
Proto.define('StructAnalogData', {
  read() {
    this.UInt32LE('frequency');
    this.UInt8('attenuator');
  },
  write({
    frequency,
    attenuator
  }) {
    this.UInt32LE(frequency)
    this.UInt8(attenuator)
  }
})



structs[0x05] = 'StructDeviceConfig'
Proto.define('StructDeviceConfig', {
  read() {
    this.UInt8('accFrequency');
    this.UIntFloatLE('adcFrequency');
  },
  write({
    accFrequency,
    adcFrequency
  }) {
    this.UInt8(accFrequency);
    this.UIntFloatLE(adcFrequency);
  }
})

frameType[0x02] = 'WriteData'
Proto.define('WriteData', {
  read() {
    this.UInt8('structId')
    this.UInt16LE('structSize')
    this[structs[this.context.structId]]()
  },
  write({
    structId,
    data
  }) {
    let begin = this.offset
    this.UInt8(0x02)
    let size = this.offset
    this.UInt16LE(0)
    this.UInt8(structId)
    this.UInt16LE(0)

    let structBegin = this.offset
    this[structs[structId]](data)
    let end = this.offset

    this.offset = size
    this.UInt16LE(end - begin)
    this.UInt8(structId)
    this.UInt16LE(end - structBegin)
    this.offset = end
  }
});

frameType[0x01] = 'ReadData'
Proto.define('ReadData', {
  read() {
    this.UInt8('structId')
    this.UInt16LE('structSize')
    this[structs[this.context.structId]]()
  },
  write({
    structId
  }) {
    this.UInt8(0x01)
    this.UInt16LE(4)
    this.UInt8(structId)
  }
});


frameType[0x82] = 'WriteDataResponse'
Proto.define('WriteDataResponse', {
  read() {
    this.UInt8('structId')
    this.UInt8('status')
  },
  write({
    structId,
    status
  }) {
    let begin = this.offset
    this.UInt8(0x02)
    this.UInt16LE(5)
    this.UInt8(structId)
    this.UInt8(status)
  }
});

frameType[0x81] = 'ReadDataResponse'
Proto.define('ReadDataResponse', {
  write({
    structId,
    data
  }) {
    let begin = this.offset
    this.UInt8(0x81)
    let size = this.offset
    this.UInt16LE(0)
    this.UInt8(structId)
    let structSize = this.offset
    this.UInt16LE(0)
    let structBegin = this.offset
    this[structs[structId]](data)
    let end = this.offset
    this.UInt16LE(end - structBegin)

    this.offset = size
    this.UInt16LE(end - begin)
    this.offset = structSize
    this.UInt16LE(end - structBegin)
    this.offset = end
  },
  read: function () {
    this.UInt8('structId')
    this.UInt16LE('structSize')
    let id = this.context.structId
    if (structs.hasOwnProperty(id)) {
      this[structs[id]]()
    } else {
      throw "Unknown struct"
    }
  }
});

frameType[0x10] = 'MeasurementFrame'
Proto.define('MeasurementFrame', {
  write ({type, timedelta, delta, samples}) {
    this.UInt8(0x10)
    this.UInt16LE(12 + samples.length * 3 * 2)
    this.UInt32LE(timestamp)
    this.UInt16LE(delta)
    this.UInt16LE(samples.length)
    this.UInt8(type)
    this.loop(samples, this.MeasureStamp)
  },
  read: function () {
    this.UInt32LE('timestamp')
    // this.context.timestamp = new Date(this.context.timestamp * 1000)
    this.UInt16LE('delta')
    this.UInt16LE('count')
    this.UInt8('type')
    this.loop('items', this.MeasureStamp, this.context.count)
  }
});

Proto.define('Frame', {
  read: function () {
    let begin = this.offset
    this.UInt8('fn')
    this.UInt16LE('frameSize')
    let fn = this.context.fn
    if (this.hasOwnProperty(frameType[fn])) {
      // console.log(frameType[fn])
      this[frameType[fn]]('data')
    } else {
      throw `Unknown frame ${fn} at ${begin}`
    }
  }
})

Proto.define('Frames', {
  read: function () {
    let arr = []
    do {
      let begin = this.offset
      try {
        let ptr = this.Frame('frame')
        arr.push(this.context.frame)
        if (this.buffer.length <= this.offset + 1) break
      } catch (e) {
        console.error(e)
        break
      }
    } while (true)
    return arr
  }
})



Proto.define('SysCallFrame', {
  write: function (ssid, password, privMode) {
    return createWiFiConfigFrame(ssid, password, privMode);
  },
  read: function () {
    this.UInt8('fn')
    this.UInt8('frameSize')
    this.UInt8('responseCode')
  }
})

Proto.define('TestFrame', {
  read: function () {
    this.UInt8('fn')
    this.UInt16LE('frameSize')
    this.raw("frameData")
  }
  /*,
    write: function(frameId, frameDateSize, frameDataResponseSize){
      this.UInt8(frameId)// Test frame id
      this.UInt16LE(1 + 2 + 2 + frameDateSize) // frame size
      this.UInt16LE(frameDataResponseSize) // frame response: data size
      if(frameDateSize != 0){
        var testData = Array(frameDateSize)
        testData.fill(0xff)
        this.raw(testData)
      }
    }*/
})

function keyValueSwap(obj) {
  return Object.keys(obj).reduce((map, id) => {
    map[obj[id]] = id
    return map
  }, {})
}

Proto.prototype.structs = keyValueSwap(structs)
Proto.prototype.frameTypes = keyValueSwap(frameType)


Proto.prototype.parse = function (stream) {
  let read = this.read(stream)
  let frames = []
  while (read.offset < stream.length) {
    let begin = read.offset
    try {
      let frame = read.Frame().result
      frames.push(frame)
    } catch (e) {
      read.offset = begin
      break
    }
  }
  return {
    frames,
    rest: stream.slice(stream.offset)
  }
}


module.exports = Proto