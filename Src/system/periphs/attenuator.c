/*
 * attenuator.c
 *
 *  Created on: 10.05.2018
 *      Author: jkonieczny
 */

#include <stdint.h>
#include "main.h"
#include <spi.h>
#include <gpio.h>

extern SPI_HandleTypeDef hspi3;

#define Select() HAL_GPIO_WritePin(SPI3_nCS_GPIO_Port, SPI3_nCS_Pin, GPIO_PIN_SET)
#define Deselect() HAL_GPIO_WritePin(SPI3_nCS_GPIO_Port, SPI3_nCS_Pin, GPIO_PIN_RESET)

HAL_StatusTypeDef ATT_Init(uint8_t value) {
	HAL_StatusTypeDef status = HAL_OK;
	return status;
}

HAL_StatusTypeDef ATT_PowerOff(uint8_t value) {
	HAL_StatusTypeDef status = HAL_OK;
	return status;
}


HAL_StatusTypeDef ATT_Set(uint8_t value) {
	HAL_StatusTypeDef status = HAL_OK;
	for (int i = 0; i < 3; i++) {
		Select();
		status |= HAL_SPI_Transmit(&hspi3, &value, 1, 100);
		Deselect();
		for(volatile int i=0; i<10000; i++);
	}
	Select();
	uint8_t response = 0;
	status |= HAL_SPI_Receive(&hspi3, &response, 1, 100);
	Deselect();
#ifdef DEBUG_PRINT
	if (value != response) {
		printf("[ATT] Can't configure attenuators\n");
	}
#endif
	return status;
}

