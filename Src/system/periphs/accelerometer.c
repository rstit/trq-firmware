/*
 * accelerometer.c
 *
 *	PC0 INT1 -- data ready to read
 *	PC1 INT2 -- device moved
 *
 *  Created on: 10.05.2018
 *      Author: jkonieczny
 */

#include <stdint.h>
#include <i2c.h>
#include <gpio.h>
#include <measurements.h>

#include "all.h"
#include "utils.h"

extern I2C_HandleTypeDef hi2c1;

#define ACC_100Hz (0b011<<3)
#define ACC_50Hz  (0b100<<3)
#define ACC_12Hz  (0b101<<3)

float lastAccValues[3];

#define Address 0x1C<<1

inline
HAL_StatusTypeDef ACC_WriteRegister(PRegister reg, void *data) {
	return HAL_I2C_Mem_Write(&hi2c1, Address, reg.address, 1, data, reg.length, 100);
}
inline
HAL_StatusTypeDef ACC_WriteByte(PRegister reg, uint8_t data) {
	return HAL_I2C_Mem_Write(&hi2c1, Address, reg.address, 1, &data, 1, 100);
}

inline
HAL_StatusTypeDef ACC_ReadRegister(PRegister reg, void *data) {
	return HAL_I2C_Mem_Read(&hi2c1, Address, reg.address, 1, data, reg.length, 100);
}

HAL_StatusTypeDef ACC_Reset() {
	volatile HAL_StatusTypeDef result = HAL_OK;
	HAL_StatusTypeDef ready = HAL_I2C_IsDeviceReady(&hi2c1, Address, 10, 1000);
	if (ready != HAL_OK) {
		printf("[ACC] Not ready: %d\r\n", ready);
		return HAL_ERROR;
	}
	uint8_t instr = 0x40;
	result = ACC_WriteRegister(ACC_CTRL_REG2, &instr);
	uint8_t reset = 0x40;
	do {
		ACC_ReadRegister(ACC_CTRL_REG2, &reset);
	} while(reset & 0x40);

//	result = ACC_WriteByte(ACC_CTRL_REG2, 0b00000010); //hi resolution;
	result = ACC_WriteByte(ACC_CTRL_REG3, 0b00000000);

	return result;
}

HAL_StatusTypeDef ACC_Active() {
	return ACC_WriteByte(ACC_CTRL_REG1, 0b00011001);
}

HAL_StatusTypeDef ACC_SetMotionListening() {
	volatile HAL_StatusTypeDef result = HAL_OK;
	int freq = ACC_12Hz;
	switch(deviceConfig.accFrequency) {
	case 0:
		freq = ACC_12Hz;
		break;
	case 1:
		freq = ACC_50Hz;
		break;
	case 2:
		freq = ACC_100Hz;
		break;
	}
	result |= ACC_WriteByte(ACC_CTRL_REG1, freq);
	result |= ACC_WriteByte(ACC_FIFO, 0);
	result |= ACC_WriteByte(ACC_MT_CFG,  0b01011000); // xy
	result |= ACC_WriteByte(ACC_XYZ_CFG, 0b00000010); // +-8g
	result |= ACC_WriteByte(ACC_MT_THS,  0b00001000); // 0.5g
	result |= ACC_WriteByte(ACC_MT_COUNT, 5); // 50ms

	//irqs
	result |= ACC_WriteByte(ACC_CTRL_REG4, 0b00000100); // Motion interrupt only
	result |= ACC_WriteByte(ACC_CTRL_REG5, 0b01000000);
	//active mode
	result |= ACC_WriteByte(ACC_CTRL_REG1, freq | 1);
#ifdef DEBUG_PRINT
	if (result == HAL_OK) {
			printf("[ACC] Motion detect mode\r\n");
		} else {
			printf("[ACC] Error setting motion detect mode\r\n");
		}
#endif
	return result;
}


HAL_StatusTypeDef ACC_SetMeasurementMode() {
	volatile HAL_StatusTypeDef result = HAL_OK;
	int freq = ACC_12Hz;
	switch(deviceConfig.accFrequency) {
	case 0:
		freq = ACC_12Hz;
		break;
	case 1:
		freq = ACC_50Hz;
		break;
	case 2:
		freq = ACC_100Hz;
		break;
	}
	result |= ACC_WriteByte(ACC_CTRL_REG1, freq);
	result |= ACC_WriteByte(ACC_FIFO, 0b10000000 | 20);
	result |= ACC_WriteByte(ACC_XYZ_CFG, 0x00 | (1<<4)); // 2g
	// irqs
	result |= ACC_WriteByte(ACC_CTRL_REG4, 0b01000000); // Data interrupg
	result |= ACC_WriteByte(ACC_CTRL_REG5, 0b00000000);
	//active mode
	result |= ACC_WriteByte(ACC_CTRL_REG1, freq | 1);
#ifdef DEBUG_PRINT
	if (result == HAL_OK) {
		printf("[ACC] Measurement mode\r\n");
	} else {
		printf("[ACC] Error setting measurement mode\r\n");
	}
#endif
	return result;
}



HAL_StatusTypeDef ACC_Init() {
	if (ACC_Reset() != HAL_OK) {
		printf("[ACC] reset error\n");
	}

	volatile HAL_StatusTypeDef result = HAL_ERROR;

	result = ACC_SetMotionListening();
#ifdef DEBUG_PRINT
	if (result != HAL_OK) {
		printf("[ACC] Accelerometer error\r\n");
	} else {
		printf("[ACC] Accelerometer configured\r\n");
	}
#endif
	return result;
}

HAL_StatusTypeDef ACC_Start() {
	volatile HAL_StatusTypeDef result = HAL_OK;
	buffers_nextAccBuffer();
	result = ACC_SetMeasurementMode();
#ifdef DEBUG_PRINT
	if (result == HAL_OK) {
		printf("[ACC] start\r\n");
	} else {
		printf("[ACC] error\r\n");
	}
#endif
	return result;
}

HAL_StatusTypeDef ACC_Stop() {
	HAL_StatusTypeDef result = HAL_OK;
	MeasurementFrame *frame = currentAccBuffer;
	currentAccBuffer = 0;
	printf("[ACC] Read at stop\r\n");
	ACC_ReadData();
	result = ACC_SetMotionListening();
	return result;
}

void ACC_Send() {
	buffers_send(currentAccBuffer);
	buffers_nextAccBuffer();
}

void ACC_ReadData() {
	ACC_Status status;
	volatile HAL_StatusTypeDef statusResult = ACC_ReadRegister(ACC_STATUS, &status);
	printf("[ACC] count = %d\n", status.cnt);
	buffers_nextAccBuffer();
	if (currentAccBuffer == 0) {
		printf("[ACC] No buffer\r\n");
		uint16_t buf[21 * 3];
		int count = status.cnt;
		if (count > 20) {
			count = 20;
		}
		HAL_StatusTypeDef res = HAL_I2C_Mem_Read(&hi2c1, Address, 0x01, 1,
				buf, count * 3 *sizeof(uint16_t), 100);
		buffers_nextAccBuffer();
		return;
	}
	currentAccBuffer->count = status.cnt;
	currentAccBuffer->delta = 80;
	HAL_StatusTypeDef res = HAL_I2C_Mem_Read(&hi2c1, Address, 0x01, 1,
			currentAccBuffer->values, status.cnt * 3 *sizeof(uint16_t), 100);

	if (res == HAL_OK) {
		for (int i = 0; i < status.cnt; i++) {
			/* mock ACC data
			currentAccBuffer->values[i][0] = 0xaa;
			currentAccBuffer->values[i][1] = 0xbb;
			currentAccBuffer->values[i][2] = 0xcc;
			*/
			for (int j = 0; j < 3; j++) {
				int16_t v = currentAccBuffer->values[i][j];
				currentAccBuffer->values[i][j] = ((v & 0xff) << 8)
						| ((v & 0xff00) >> 8);
			}
		}
		lastAccValues[0] = currentAccBuffer->values[0][0] / 32768.0f;
		lastAccValues[1] = currentAccBuffer->values[0][1] / 32768.0f;
		lastAccValues[2] = currentAccBuffer->values[0][2] / 32768.0f;
		currentAccBuffer->frameSize = 3 + 9 + status.cnt * sizeof(int16_t) * 3;
		frame_send(currentAccBuffer, currentAccBuffer->frameSize);
	} else {
		printf("[ACC] ReadData error\r\n");
	}
//	buffers_nextAccBuffer();
}

uint32_t accIrqTime = 0;

/**
 * @brief Accelerometer's FIFO ready
 */
void ACC_IrqHandler() {
	uint8_t source = 0;
	uint32_t start = HAL_GetTick();
	HAL_StatusTypeDef result = ACC_ReadRegister(ACC_INT_SOURCE, &source);
	if (result != HAL_OK) {
		printf("Can't read int source %X\r\n", result);
		return;
	}
	if ((source & 0x40) == 0x40) {
		// fifo ready
		ACC_ReadData();
	}
	if ((source & 0x04) == 0x04) {
		// freefall
		uint8_t mt;
		ACC_ReadRegister(ACC_MT_SRC, &mt);
		if (mt & 0x80) {
			power_KeepAlive();
		}
	}
	if ((source & 0x10) == 0x10) {
		// landscape change
	}
	uint32_t end = HAL_GetTick();
	accIrqTime = end - start;
}
