
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __WIFI_MODULE_CONF_H
#define __WIFI_MODULE_CONF_H

/* Includes ------------------------------------------------------------------*/
#include "wifi_conf.h"
    
#include "stm32f3xx_hal.h"
#include "stm32f3xx_hal_rcc.h"
#include "stm32f3xx_hal_rcc_ex.h"
#include "stm32f3xx_hal_dma.h"


#endif //__WIFI_MODULE_CONF_H
