#include "wifi.h"
#include "wifi_ap.h"
#include "usart.h"


char *WIFI_TCPServer_Protocol = "t";//t -> tcp , s-> secure tcp
uint32_t WIFI_TCPServer_PortNumber = 3200;



void WIFI_PrintStatus(WiFi_Status_t status)
{
	switch ((int)status)
	{
		case WiFi_MODULE_SUCCESS				: printf("\r\n WiFi_MODULE_SUCCESS\r\n");
		break;
		case WiFi_TIME_OUT_ERROR				: printf("\r\n WiFi_TIME_OUT_ERROR\r\n");
		break;
		case WiFi_MODULE_ERROR					: printf("\r\n WiFi_MODULE_ERROR\r\n");
		break;
		case WiFi_HAL_OK								: printf("\r\n WiFi_HAL_OK\r\n");
		break;
		case WiFi_NOT_SUPPORTED					: printf("\r\n WiFi_NOT_SUPPORTED\r\n");
		break;
		case WiFi_NOT_READY							: printf("\r\n WiFi_NOT_READY\r\n");
		break;
		case WiFi_SCAN_FAILED						: printf("\r\n WiFi_SCAN_FAILED\r\n");;
		break;
		case WiFi_AT_CMD_BUSY 					: printf("\r\n WiFi_AT_CMD_BUSY\r\n");;
		break;
		case WiFi_SSID_ERROR						: printf("\r\n WiFi_SSID_ERROR\r\n");;
		break;
		case WiFi_SecKey_ERROR					: printf("\r\n WiFi_SecKey_ERROR\r\n");;
		break;
		case WiFi_CONFIG_ERROR					: printf("\r\n WiFi_CONFIG_ERROR\r\n");;
		break;
		case WiFi_STA_MODE_ERROR				: printf("\r\n WiFi_STA_MODE_ERROR\r\n");;
		break;
		case WiFi_AP_MODE_ERROR					: printf("\r\n WiFi_AP_MODE_ERROR\r\n");;
		break;
		case WiFi_AT_CMD_RESP_ERROR			: printf("\r\n WiFi_AT_CMD_RESP_ERROR\r\n");;
		break;
		case WiFi_AT_FILE_LENGTH_ERROR	: printf("\r\n WiFi_AT_FILE_LENGTH_ERROR\r\n");;
		break;
		case WiFi_HAL_UART_ERROR 				: printf("\r\n WiFi_HAL_UART_ERROR\r\n");;
		break;
		case WiFi_IN_LOW_POWER_ERROR		: printf("\r\n WiFi_IN_LOW_POWER_ERROR\r\n");;
		break;
		case WiFi_HW_FAILURE_ERROR			: printf("\r\n WiFi_HW_FAILURE_ERROR\r\n");;
		break;
		case WiFi_HEAP_TOO_SMALL_WARNING: printf("\r\n WiFi_HEAP_TOO_SMALL_WARNING\r\n");;
		break;
		case WiFi_STACK_OVERFLOW_ERROR	: printf("\r\n WiFi_STACK_OVERFLOW_ERROR\r\n");;
		break;
		case WiFi_HARD_FAULT_ERROR			: printf("\r\n WiFi_HARD_FAULT_ERROR\r\n");;
		break;
		case WiFi_MALLOC_FAILED_ERROR		: printf("\r\n WiFi_MALLOC_FAILED_ERROR\r\n");;
		break;
		case WiFi_INIT_ERROR						: printf("\r\n WiFi_INIT_ERROR\r\n");;
		break;
		case WiFi_POWER_SAVE_WARNING		: printf("\r\n WiFi_POWER_SAVE_WARNING\r\n");;
		break;
		case WiFi_SIGNAL_LOW_WARNING		: printf("\r\n WiFi_SIGNAL_LOW_WARNING\r\n");;
		break;
		case WiFi_JOIN_FAILED						: printf("\r\n WiFi_JOIN_FAILED\r\n");;
		break;
		case WiFi_SCAN_BLEWUP						: printf("\r\n WiFi_SCAN_BLEWUP\r\n");;
		break;
		case WiFi_START_FAILED_ERROR		: printf("\r\n WiFi_START_FAILED_ERROR\r\n");;
		break;
		case WiFi_EXCEPTION_ERROR				: printf("\r\n WiFi_EXCEPTION_ERROR\r\n");;
		break;
		case WiFi_DE_AUTH								: printf("\r\n WiFi_DE_AUTH\r\n");;
		break;
		case WiFi_DISASSOCIATION				: printf("\r\n WiFi_DISASSOCIATION\r\n");;
		break;
		case WiFi_UNHANDLED_IND_ERROR		: printf("\r\n WiFi_UNHANDLED_IND_ERROR\r\n");;
		break;
		case WiFi_RX_MGMT								: printf("\r\n WiFi_RX_MGMT\r\n");;
		break;
		case WiFi_RX_DATA								: printf("\r\n WiFi_RX_DATA\r\n");;
		break;
		case WiFi_RX_UNK								: printf("\r\n WiFi_RX_UNK\r\n");;
		break;
		default											: printf("\r\n WiFi_UnknowCode: %d\r\n", status);
		break;
	}
}

// Simple API
static char buffer[512];

WiFi_Status_t WIFI_SetConfigurationValue(const char* name, uint32_t value){
	WiFi_Status_t status = WiFi_MODULE_SUCCESS;
	memset(buffer, 0x00, 512);
	int size = sprintf(buffer, AT_SET_CONFIGURATION_VALUE, name, (int)value);
	status = HAL_UART_Transmit(&huart2, (uint8_t*)buffer, size, 100);
	HAL_Delay(100);
	return status;
}

WiFi_Status_t WIFI_SetConfigurationAddress(const char* name, const char* address){
	WiFi_Status_t status = WiFi_MODULE_SUCCESS;
	memset(buffer, 0x00, 512);
	int size = sprintf(buffer, AT_SET_CONFIGURATION_ADDRESS, name, address);
	status = HAL_UART_Transmit(&huart2, (uint8_t*)buffer, size, 100);
	HAL_Delay(300);
	return status;
}

WiFi_Status_t WIFI_ExecuteCommand(const char* command){
	WiFi_Status_t status = WiFi_MODULE_SUCCESS;
	memset(buffer, 0x00, 512);
	int size = sprintf(buffer, "%s", command);
	status = HAL_UART_Transmit(&huart2, (uint8_t*)buffer, size, 100);
	HAL_Delay(300);
	return status;
}

WiFi_Status_t WIFI_ExecuteCommandWithArgInt(const char* format, uint32_t value){
	WiFi_Status_t status = WiFi_MODULE_SUCCESS;
	memset(buffer, 0x00, 512);
	int size = sprintf(buffer, format, value);
	status = HAL_UART_Transmit(&huart2, (uint8_t*)buffer, size, 100);
	HAL_Delay(300);
	return status;
}

WiFi_Status_t WIFI_ExecuteCommandWithArgString(const char* format, const char* value){
	WiFi_Status_t status = WiFi_MODULE_SUCCESS;
	memset(buffer, 0x00, 512);
	int size = sprintf(buffer, format, value);
	status = HAL_UART_Transmit(&huart2, (uint8_t*)buffer, size, 100);
	HAL_Delay(300);
	return status;
}

void WIFI_PowerUp(){
    printf("[WiFi] PowerDown...\r\n");
    // WiFi module - Power Down
    HAL_GPIO_WritePin(WLAN_nPWR_EN_GPIO_Port, WLAN_nPWR_EN_Pin, GPIO_PIN_SET);
    HAL_Delay(50);
    // WiFi module - Power Up
    HAL_GPIO_WritePin(WLAN_nPWR_EN_GPIO_Port, WLAN_nPWR_EN_Pin, GPIO_PIN_RESET);
    HAL_Delay(100);
    printf("[WiFi] PowerDown\r\n");
}

void WIFI_PowerDown(){
    HAL_GPIO_WritePin(WLAN_nPWR_EN_GPIO_Port, WLAN_nPWR_EN_Pin, GPIO_PIN_SET);
    WIFI_LED_OnOff(0);
    printf("[WiFi] PowerDown\n");
}

void WIFI_Reset(){
    printf("[WiFi] Reset ...\r\n");
    // 0. RESTORE to low - no restore settings
    HAL_GPIO_WritePin(WLAN_RESTORE_GPIO_Port, WLAN_RESTORE_Pin, GPIO_PIN_RESET);
    // 1. Set BOOT to low state
    HAL_GPIO_WritePin(WLAN_BOOT_GPIO_Port, WLAN_BOOT_Pin, GPIO_PIN_RESET);
    // 2. Set RESET to low for minimum 5ms
    HAL_GPIO_WritePin(WLAN_nRESET_GPIO_Port, WLAN_nRESET_Pin, GPIO_PIN_RESET);
    HAL_Delay(200);
    HAL_GPIO_WritePin(WLAN_nRESET_GPIO_Port, WLAN_nRESET_Pin, GPIO_PIN_SET);
    HAL_Delay(100);
    printf("[WiFi] Reset\r\n");
}

void WIFI_LED_OnOff(int offOn){
	if(offOn == 0){
		HAL_GPIO_WritePin(USER_LED_WIFI_GPIO_Port, USER_LED_WIFI_Pin, GPIO_PIN_RESET);
	}else{
		HAL_GPIO_WritePin(USER_LED_WIFI_GPIO_Port, USER_LED_WIFI_Pin, GPIO_PIN_SET);
	}
}

void WIFI_LED_Toggle(){
	HAL_GPIO_TogglePin(USER_LED_WIFI_GPIO_Port, USER_LED_WIFI_Pin);
}

void WIFI_RestoreDefaultSettingsByPinState(){

	HAL_GPIO_WritePin(WLAN_RESTORE_GPIO_Port, WLAN_RESTORE_Pin, GPIO_PIN_SET);
	WIFI_PowerUp();
	WIFI_Reset();
	printf("Save default settings ...");
	UART_Configuration(WiFi_SPEED_DEFAULT);
	WiFi_Status_t status = WiFi_MODULE_SUCCESS;
	HAL_Delay(1000);

	//Info: pdf: UM1695 (DM00100306)
	//Setup transmission settings
	status |= WIFI_SetConfigurationValue(CONSOLE1_HWFC, 1);
	status |= WIFI_SetConfigurationValue(CONSOLE1_SPEED, WiFi_SPEED_DEFAULT);
	//Setup power mode = ACTIVE
	status |= WIFI_SetConfigurationValue(WIFI_POWERSAVE, 0);
	status |= WIFI_SetConfigurationValue(WIFI_SLEEP_ENABLED, 0);
	status |= WIFI_SetConfigurationValue(WIFI_STANDBY_ENABLED, 0);
	status |= WIFI_SetConfigurationValue(WIFI_TX_POWER, 10);
	// Save settings into internal Flash
	status |= WIFI_ExecuteCommand(AT_SAVE_CURRENT_SETTING);
	printf("end with save status: %d\r\n", status);
	HAL_Delay(2000);
	data_ClearWiFiConfig();
	HAL_Delay(500);
	// Reset (0 = switch to the active state and reset the device)
	status |= WIFI_ExecuteCommand(AT_SAVE_CURRENT_SETTING);
	status |= WIFI_ExecuteCommandWithArgInt(AT_SET_POWER_STATE, 0);

	UART_Configuration(WiFi_SPEED_DEFAULT);
	HAL_Delay(3000);
	HAL_GPIO_WritePin(WLAN_RESTORE_GPIO_Port, WLAN_RESTORE_Pin, GPIO_PIN_RESET);
	HAL_UART_AbortReceive(&huart2);
	HAL_UART_AbortTransmit(&huart2);

	printf("End restoring default settings\r\n");
}

WiFi_Status_t WIFI_PreSetup () {
	volatile WiFi_Status_t status = WiFi_MODULE_SUCCESS;
	status |= WIFI_SetConfigurationValue("ip_sockd_timeout", 5);
	status |= WIFI_SetConfigurationValue(IP_USE_HTTPD, 0);
	status |= WIFI_ExecuteCommandWithArgInt(AT_HTTPD, 0);
	status |= WIFI_SetConfigurationValue(WIFI_TX_POWER, 5);

	status |= SET_Configuration_Value(WIFI_HT_MODE, 0);

//	status |= WIFI_ExecuteCommand("AT+S.SCFG=wifi_opr_rate_mask,1\r");
	status |= WIFI_SetConfigurationValue("wifi_opr_rate_mask", 0b1);
	//	status |= WIFI_SetConfigurationValue("wifi_bas_rate_mask", 0b1111);
	status |= WIFI_SetConfigurationValue("wifi_bas_rate_mask", 1);
	status |= WIFI_SetConfigurationValue("wifi_txfail_thresh", 10);
	status |= WIFI_SetConfigurationValue("wifi_add_tim_ie", 1);

	return status;
}

WiFi_Status_t WIFI_Module_Init(uint32_t uart_speed){
	volatile WiFi_Status_t status = WiFi_MODULE_SUCCESS;

	// Restore default settings
//	HAL_Delay(1000);
//	status |= WIFI_ExecuteCommand(AT_RESTORE_DEFAULT_SETTING);
//	HAL_Delay(1000);

	WIFI_ExecuteCommand("AT+S.STS=reset_reason\r");

	//Setup transmission settings
	status |= WIFI_SetConfigurationValue(CONSOLE1_HWFC, 1);
	status |= WIFI_SetConfigurationValue(CONSOLE1_SPEED, uart_speed);
	UART_Configuration(uart_speed);
	//Setup power mode = ACTIVE
	status |= WIFI_SetConfigurationValue(WIFI_POWERSAVE, 0);
	status |= WIFI_SetConfigurationValue(WIFI_SLEEP_ENABLED, 0);
	status |= WIFI_SetConfigurationValue(WIFI_STANDBY_ENABLED, 0);
	status |= WIFI_SetConfigurationValue(WIFI_TX_POWER, 2*6);

	// On 'n' standard
	status |= WIFI_SetConfigurationValue("wifi_ht_mode", 1); // Mode: b,g, n
	status |= WIFI_ExecuteCommand("AT+S.SCFG=wifi_opr_rate_mask,0x003FFFCF\r"); // for n mode

	status |= WIFI_SetConfigurationValue("ip_sockd_timeout", 50);

	//DHCP On
	status |= WIFI_SetConfigurationValue(IP_USE_DHCP_SERVER, 1);

	status |= WIFI_SetConfigurationValue(IP_USE_HTTPD, 0);
	status |= WIFI_ExecuteCommandWithArgInt(AT_HTTPD, 0);


	//Save settings
	status |= WIFI_ExecuteCommand(AT_SAVE_CURRENT_SETTING);
	HAL_Delay(1000);

	// Run  "framework"
	WiFi_Module_Init();
	wifi_reset();
	//Receive_Data();//Restart data reception

	return status;
}

WiFi_Status_t WIFI_Module_Enable(){
	return WIFI_ExecuteCommandWithArgInt(AT_WIFI_ENABLE, 1);
}

WiFi_Status_t WIFI_Module_Diasble(){
	return WIFI_ExecuteCommandWithArgInt(AT_WIFI_ENABLE, 0);
}

/*
WiFi_Status_t WIFI_GetConfigurationValue(const char* name){
	WiFi_Status_t status = WiFi_MODULE_SUCCESS;
	memset(buffer, 0x00, 512);
	int size = sprintf(buffer, AT_GET_CONFIGURATION_VALUE, name);
	status = HAL_UART_Transmit(&huart2, (uint8_t*)buffer, size, 100);
	memset(buffer, 0x00, 512);
	status |= HAL_UART_Receive(&huart2, buffer, 50, 1000);
	return status;
}
*/

//Simple API, end

WiFi_Status_t WIFI_GetAllConfiguration(){
	WiFi_Status_t status = WiFi_MODULE_SUCCESS;

	Reset_AT_CMD_Buffer();

	sprintf((char*)WiFi_AT_Cmd_Buff, AT_GET_ALL_CONFIGURATION);

	status = USART_Transmit_AT_Cmd(strlen((char*)WiFi_AT_Cmd_Buff));
	if(status == WiFi_MODULE_SUCCESS)
	{
	  status = USART_Receive_AT_Resp();
	}

	return status;
}

WiFi_Status_t WIFI_ExecuteCommandOld(const char* command){
	WiFi_Status_t status = WiFi_MODULE_SUCCESS;

	Reset_AT_CMD_Buffer();

	sprintf((char*)WiFi_AT_Cmd_Buff, "%s", command);

	status = USART_Transmit_AT_Cmd(strlen((char*)WiFi_AT_Cmd_Buff));
	if(status == WiFi_MODULE_SUCCESS)
	{
	  status = USART_Receive_AT_Resp();
	}

	return status;
}

void WIFI_LED_BlinkOK(uint8_t blinksNumer, uint32_t blinkPeriod){
	GPIO_PinState scanState;
	GPIO_PinState wifiState;
	scanState = HAL_GPIO_ReadPin(USER_LED_SCAN_GPIO_Port, USER_LED_SCAN_Pin);
	wifiState = HAL_GPIO_ReadPin(USER_LED_WIFI_GPIO_Port, USER_LED_WIFI_Pin);

	HAL_GPIO_WritePin(USER_LED_SCAN_GPIO_Port, USER_LED_SCAN_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(USER_LED_WIFI_GPIO_Port, USER_LED_WIFI_Pin, GPIO_PIN_RESET);
	for(int i = 0; i < blinksNumer; i++){
		HAL_Delay(blinkPeriod);
		HAL_GPIO_TogglePin(USER_LED_SCAN_GPIO_Port, USER_LED_SCAN_Pin);
		HAL_GPIO_TogglePin(USER_LED_WIFI_GPIO_Port, USER_LED_WIFI_Pin);
	}
	HAL_Delay(blinkPeriod);
	HAL_GPIO_WritePin(USER_LED_SCAN_GPIO_Port, USER_LED_SCAN_Pin, scanState);
	HAL_GPIO_WritePin(USER_LED_WIFI_GPIO_Port, USER_LED_WIFI_Pin, wifiState);
}

void WIFI_LED_BlinkFault(uint8_t blinksNumer, uint32_t blinkPeriod){
	GPIO_PinState faultState;
	GPIO_PinState wifiState;
	faultState = HAL_GPIO_ReadPin(USER_LED_FAULT_GPIO_Port, USER_LED_FAULT_Pin);
	wifiState = HAL_GPIO_ReadPin(USER_LED_WIFI_GPIO_Port, USER_LED_WIFI_Pin);

	HAL_GPIO_WritePin(USER_LED_FAULT_GPIO_Port, USER_LED_FAULT_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(USER_LED_WIFI_GPIO_Port, USER_LED_WIFI_Pin, GPIO_PIN_RESET);
	for(int i = 0; i < blinksNumer; i++){
		HAL_Delay(blinkPeriod);
		HAL_GPIO_TogglePin(USER_LED_FAULT_GPIO_Port, USER_LED_FAULT_Pin);
		HAL_GPIO_TogglePin(USER_LED_WIFI_GPIO_Port, USER_LED_WIFI_Pin);
	}
	HAL_Delay(blinkPeriod);
	HAL_GPIO_WritePin(USER_LED_FAULT_GPIO_Port, USER_LED_FAULT_Pin, faultState);
	HAL_GPIO_WritePin(USER_LED_WIFI_GPIO_Port, USER_LED_WIFI_Pin, wifiState);
}

/* WiFi users callback's */
void ind_wifi_socket_data_received(int8_t callback_server_id, int8_t socket_id,
		uint8_t * data_ptr, uint32_t message_size, uint32_t chunk_size, WiFi_Socket_t socket_type){
	WIFI_Callbacks.data_received(callback_server_id, socket_id, data_ptr, message_size, chunk_size, socket_type);
}

void ind_wifi_error(WiFi_Status_t error_code){
	WIFI_Callbacks.error(error_code);
}

void ind_wifi_connected(){
	WIFI_Callbacks.connected();
}




