# trq-firmware

# Hardware specification

Battery is 18650CA-1S-3J
Li-ION 3.7V 2250mAh (8.33Wh)
 * Nominal voltage is 3.7V
 * Battery capacity is 2250mAh (0.2C discharged from 4.2V to 2.75V)
 * Charge voltage of 4.2V
 * Maximum charge current is 2200mA
 * Maximum continuous discharge current is 2200mA
 * Charging working temperature range from 0°C to 45°C
 * Discharging working temperature range from -20°C to 60°C
 * Approximate battery weight is 50g
 
 