#ifndef WIFI_AP_H_
#define WIFI_AP_H_

#include "wifi.h"

typedef enum {
	wifiAPNameGenerated = 0, wifiAPNoMACAddress
} WiFiAPName_t;


WiFi_Status_t WIFI_FillMACAddress();
WiFiAPName_t WIFI_GenerateAPName();
const char* WIFI_GetAPName();



#endif /* WIFI_AP_H_ */
