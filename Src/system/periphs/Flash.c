#include "main.h"
#include "all.h"

extern SPI_HandleTypeDef hspi1;

typedef struct FlashBlock {
	uint32_t size;
	uint32_t crc;
	uint8_t data[0];
} FlashBlock;

#define SPI &hspi1

inline uint32_t mem_addr(uint32_t address) {
	return ((address & 0xff0000) >> 16) | ((address & 0x00ff00))
			| ((address & 0x0000ff) << 16);
}

typedef struct Mem_Instr {
	uint8_t instruction;
	uint32_t address :24;
	uint32_t x;
}__attribute__((packed)) Mem_Instr;


HAL_StatusTypeDef Flash_Short(uint32_t instr) {
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_RESET);
	HAL_StatusTypeDef result = HAL_SPI_Transmit(SPI, (void*) &instr, 1, 10);
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_SET);
	return result;
}


inline HAL_StatusTypeDef Flash_Transmit(void *ptr, int len) {
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_RESET);
	for (volatile int i = 0; i < 10000; i++)
			;
	HAL_StatusTypeDef result = HAL_SPI_Transmit(SPI, ptr, len, 10);
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_SET);
	return result;
}

HAL_StatusTypeDef Flash_WREN() {
	uint8_t wren = 0x06;
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_RESET);
	HAL_StatusTypeDef result = HAL_SPI_Transmit(SPI, &wren, 1, 10);
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_SET);
	return result;
}

uint32_t Flash_ReadJedec() {
	uint32_t id = 0x9f;
	uint32_t data = 0x11111111;
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_RESET);
	for (volatile int i = 0; i < 10000; i++)
		;
	HAL_SPI_TransmitReceive(SPI, &id, &data, 4, 10);
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_SET);
	return data;
}

inline HAL_StatusTypeDef Flash_Receive(void *ptr, int len) {
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_RESET);
	HAL_StatusTypeDef result = HAL_SPI_Receive(SPI, ptr, len, 10);
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_SET);
	return result;
}

uint16_t Flash_GetStatus(int conf) {
	uint8_t instr = conf ? 0x35 : 0x05;
	uint8_t status;
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_RESET);
	HAL_StatusTypeDef result = HAL_SPI_Transmit(SPI, (void*) &instr, 1, 10);
	result |= HAL_SPI_Receive(SPI, (void*) &status, 1, 10);
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_SET);
	if (result == HAL_OK) {
		return status;
	} else {
		return 0xffff;
	}
}

int Flash_BUSY() {
	uint8_t status;
	uint32_t now = HAL_GetTick();
	do {
		status = Flash_GetStatus(0);
		if (HAL_GetTick() - now > 200) {
			return 0;
		}
	} while (status & 0x81);
	return 1;
}

HAL_StatusTypeDef Flash_ResetEnable() {
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_SET);
	uint8_t cmdResetEnable = 0x66;
	uint8_t cmdReset = 0x99;
	HAL_GPIO_WritePin(MEM_nPWR_EN_GPIO_Port, MEM_nPWR_EN_Pin, GPIO_PIN_RESET);
	HAL_Delay(10);
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(SPI, (void*) &cmdResetEnable, 1, 10);
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_SET);
	HAL_Delay(5);
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(SPI, (void*) &cmdReset, 1, 10);
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_SET);
	return HAL_OK;
}

HAL_StatusTypeDef Flash_BlockErase(uint32_t address) {
	HAL_StatusTypeDef result = HAL_OK;
	if (address % 64 != 0) {
		return HAL_ERROR;
	}
	Mem_Instr instr;
	instr.instruction = 0xD8;
	instr.address = mem_addr(address);
	instr.x = 0;
	Flash_BUSY();
	Flash_Transmit((void*) &instr, 1 + 3);

	return result;
}

HAL_StatusTypeDef Flash_ReadBlock(FlashFragment *fragment) {
	uint32_t checksum = fragment->salt;
	uint32_t readedChecksum = 0;
	volatile HAL_StatusTypeDef result = HAL_OK;
	Mem_Instr instr;
	instr.instruction = 0x03;
	instr.address = mem_addr(fragment->flashOffset);
	printf("[MEM] Read from block %d\n", fragment->flashOffset);
	instr.x = 0;
	Flash_BUSY();
	memset(fragment->ptr, 0, fragment->blockLength);
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_RESET);
	for (volatile int i = 0; i < 10000; i++)
			;
	result = HAL_SPI_Transmit(SPI, (void*) &instr, 4, 10);
	result = HAL_SPI_Receive(SPI, (void*) &readedChecksum, 4, 10);
	result = HAL_SPI_Receive(SPI, fragment->ptr, fragment->blockLength, 10);
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_SET);

	uint8_t *u8 = (uint8_t*)fragment->ptr;
	for (int i = 0; i < fragment->blockLength; i++) {
		checksum ^= u8[i] << ((i % 4) * 8);
	}
	if (result != HAL_OK) {
		printf("[MEM] Load error\r\n");
	}
	if (checksum == readedChecksum) {
		return HAL_OK;
	} else {
		printf("[MEM] Wrong checksum: %04X != %04X\r\n", checksum, readedChecksum);
		return HAL_ERROR;
	}
}

HAL_StatusTypeDef Flash_WriteBlock(FlashFragment *fragment) {
	// TODO: check if it's necessary, spare FLASH write limit!
	uint32_t checksum = fragment->salt;
	Mem_Instr instr;
	instr.instruction = 0x02;
	instr.address = mem_addr(fragment->flashOffset);

	Flash_Short(0x98); // unlock all
	Flash_WREN();

	HAL_StatusTypeDef result = HAL_OK;
	result = Flash_BlockErase(fragment->flashOffset);
	uint8_t *u8 = (uint8_t*)fragment->ptr;
	for (int i = 0; i < fragment->blockLength; i++) {
		checksum ^= u8[i] << ((i % 4) * 8);
	}
	instr.x = checksum;
	Flash_BUSY();
	Flash_Short(0x98);
	Flash_BUSY();
	HAL_Delay(10);
	Flash_WREN();
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_RESET);
	for (volatile int i = 0; i < 10000; i++)
			;
	result = HAL_SPI_Transmit(SPI, (void*) &instr, 4, 10);
	result |= HAL_SPI_Transmit(SPI, (void*) &checksum, 4, 10);
	result |= HAL_SPI_Transmit(SPI, (void*) fragment->ptr,
			fragment->blockLength, 10);
	HAL_GPIO_WritePin(MEM_SPI_NSS_GPIO_Port, MEM_SPI_NSS_Pin, GPIO_PIN_SET);

	volatile uint8_t status = Flash_GetStatus(0);

	return result;
}
