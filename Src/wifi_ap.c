#include "wifi.h"
#include "wifi_ap.h"
#include "structures.h"

static const char* wifiAPNamePattern = "Achilles_%s";
static char wifiAPName[32];
static char wifiAPPassword[32];
static WiFi_Priv_Mode wifiAPMode = None;
static uint8_t wifiAPChannel = 3;

char receivedCommandBuffer[1024];

char wifi_ip_addr[20];
char wifiMACAddr[18];

WiFi_Status_t WIFI_ExecuteCommand(const char* command);

static wifi_state_t wifi_state;

static void remchar(char *s, char chr);

/* user callback 8*/
void ap_ind_wifi_error(WiFi_Status_t error_code);
void ap_ind_wifi_socket_data_received(int8_t callback_server_id, int8_t socket_id,
		uint8_t * data_ptr, uint32_t message_size, uint32_t chunk_size, WiFi_Socket_t socket_type);
void ap_ind_wifi_connected();

void system_SendPendingFrames();

extern WIFI_Callbacks_t WIFI_Callbacks;
WIFI_Callbacks_t AP;


static void WIFI_AP_InitCallbacks(){
	AP.error = ap_ind_wifi_error;
	AP.data_received = ap_ind_wifi_socket_data_received;
	AP.connected = ap_ind_wifi_connected;

	WIFI_Callbacks = AP;
}

WiFi_Status_t WIFI_FillMACAddress() {
	memset(wifiMACAddr, 0, sizeof(wifiMACAddr) / sizeof(wifiMACAddr[0]));
	WiFi_Status_t status = wifi_get_MAC_address((uint8_t*) wifiMACAddr);
	remchar(wifiMACAddr, ':');
	memset(&wifiMACAddr[12], 0x00, 6);
	return status;
}

WiFiAPName_t WIFI_GenerateAPName() {
	if (wifiMACAddr[0] == 0x00) {
		return wifiAPNoMACAddress;
	}
	memset(wifiAPName, 0x00, sizeof(wifiAPName) / sizeof(wifiAPName[0]));
	// get last 4 pairs from MAC address;
	sprintf(wifiAPName, wifiAPNamePattern, wifiMACAddr);
	// Use four last bytes from MAC address (with last \0)

	return wifiAPNameGenerated;
}

const char* WIFI_GetAPName() {
	if (wifiAPName[0] == 0x00) {
		return NULL;
	}
	return wifiAPName;
}

static void remchar(char *s, char chr)
{
   int i, j = 0;
   for ( i = 0; s[i] != '\0'; i++ ) /* 'i' moves through all of original 's' */
   {
      if ( s[i] != chr )
      {
         s[j++] = s[i]; /* 'j' only moves after we write a non-'chr' */
      }
   }
   s[j] = '\0'; /* re-null-terminate */
}

WiFi_Status_t WIFI_AP_GetSettings(void){

	//generate AP name from MAC address
 	WiFi_Status_t macStatus = WIFI_FillMACAddress();

 	if(macStatus != WiFi_MODULE_SUCCESS){
 		printf("Can't get MAC address\r\n");
 		return macStatus;
 	}

 	WiFiAPName_t apNameStatus = WIFI_GenerateAPName();
 	if(apNameStatus != wifiAPNameGenerated){
 		printf("Can't generate AP name.\r\n");
 		return WiFi_MODULE_ERROR;
 	}

 	wifiAPMode = WPA_Personal;
 	const char* tmpPassword = "AchillesPass";

 	memcpy(wifiAPPassword, tmpPassword, strlen(tmpPassword));

 	printf("\r\n#AP SSID: %s\r\n", wifiAPName);
 	printf("#AP Pass: %s\r\n", wifiAPPassword);
 	printf("#AP Mode: %d\r\n", wifiAPMode);
 	printf("#AP Channel: %d\r\n", wifiAPChannel);

	return WiFi_MODULE_SUCCESS;
}

void WIFI_InitState(){
	  wifi_state = wifi_state_idle;
}

static WiFi_Status_t WIFI_AP_Init(){
	WiFi_Status_t status = WiFi_MODULE_SUCCESS;
	unsigned char wifiAPPasswordHex[33];

	if(wifiAPMode == WEP){
		//convert WiFiPassword to hex format (for WEP)
		memset(wifiAPPasswordHex, 0x00, sizeof(wifiAPPasswordHex));
		int i, j;
		for(i=0, j=0; i < strlen(wifiAPPassword); i++, j+=2)
		{
			sprintf((char*)wifiAPPasswordHex+j,"%x", wifiAPPassword[i]);
		}
		wifiAPPasswordHex[j]='\0';
		//printf("AP pass: %s\r\n", wifiAPPasswordHex);

		for(i = j; i < 32; i+=2){
			sprintf((char*)wifiAPPasswordHex+i,"%d%d", 0, 0);
		}
	}else{
		for(int i = 0; i < 32; i+=2){
			sprintf((char*)wifiAPPasswordHex+i,"%d%d", 0, 0);
		}
	}

	//Init AP
	status |= WIFI_SetConfigurationValue(WIFI_PRIV_MODE, wifiAPMode);

	status |= WIFI_ExecuteCommandWithArgString(AT_SET_SSID, wifiAPName);
	status |= WIFI_SetConfigurationValue(WIFI_CHANNEL_NUMBER, wifiAPChannel);

	if(wifiAPMode == None){
		status |= WIFI_SetConfigurationValue(WIFI_AUTH_TYPE, 0); // Authentication type used in STA, IBSS and MiniAP mode: 0=OpenSystem, 1=SharedKey
	}else{
		status |= WIFI_SetConfigurationValue(WIFI_AUTH_TYPE, 0); // Authentication type used in STA, IBSS and MiniAP mode: 0=OpenSystem, 1=SharedKey
	}
	if(wifiAPMode == WEP){
		status |= WIFI_SetConfigurationAddress("wifi_wep_keys[0]", (const char*)wifiAPPasswordHex);
		status |= WIFI_SetConfigurationValue("wifi_wep_key_lens", strlen(wifiAPPassword));
		status |= WIFI_SetConfigurationValue("wifi_wep_default_key", 0);
	}else if(wifiAPMode == WPA_Personal){
		status |= WIFI_ExecuteCommandWithArgString(AT_SET_SEC_KEY, wifiAPPassword);
	}
	status |= WIFI_SetConfigurationValue(WIFI_PRIV_MODE, wifiAPMode);
	status |= WIFI_SetConfigurationAddress(WIFI_IP_HOSTNAME, wifiAPName);
	status |= WIFI_SetConfigurationValue(WIFI_MODE, WiFi_MiniAP_MODE);	// 3-miniAP
	status |= WIFI_PreSetup();
	status |= WIFI_ExecuteCommand(AT_SAVE_CURRENT_SETTING);

	if(status != WiFi_MODULE_SUCCESS){
		//printf("\r\n AP Start Error: Init AP: %d\r\n", status);
		return status;
	}

	status |= WIFI_ExecuteCommand(AT_RESET);
	wifi_reset();
	//WIFI_PreSetup();

	return status;
}


WiFi_Status_t WIFI_AP_Start(){

	data_ClearWiFiConfig();

	WIFI_AP_InitCallbacks();

	WiFi_Status_t status = WiFi_MODULE_SUCCESS;
	status = WIFI_AP_GetSettings();
	if(status != WiFi_MODULE_SUCCESS){
		printf("\r\n AP Start Error: Get settings: %d\r\n", status);
		return status;
	}

	status |= WIFI_AP_Init();

	if(status != WiFi_MODULE_SUCCESS){
			printf("\r\n AP Start Error: Initialization AP: %d\r\n", status);
			return status;
	}

	WIFI_LED_OnOff(1);

	wifi_state = wifi_state_idle;
	HAL_Delay(3000);



//	WIFI_GetAllConfiguration();

	while (1){
		 switch (wifi_state)
		 {
		   case wifi_state_reset:
		   break;
		   case wifi_state_ready:
			  printf("\r\n >>AP ready.\r\n");
			  wifi_state = wifi_state_idle;
			  break;
		   case wifi_state_connected:
			printf("\r\n>>AP ready: Connected.\r\n");
			wifi_state = wifi_state_socket;
			break;
		   case wifi_state_disconnected:
			   printf("\r\n >>disconnected..\r\n");
			   wifi_state = wifi_state_idle;
			break;
			case wifi_state_socket:
				HAL_Delay(50);
				printf("\r\n>>WiFi server socket opening..\r\n");
				while (1) {
					status = wifi_socket_server_open(WIFI_TCPServer_PortNumber,
							(uint8_t *) WIFI_TCPServer_Protocol);
					if (status == WiFi_MODULE_SUCCESS) {
						printf(">>Server Socket Open OK \r\n");
						//WIFI_ExecuteCommand(AT_SHOW_CURRENT_SETTING);
						break;
					} else {
						printf(">>Server Socket Open FAIL %d\r\n", status);
						HAL_Delay(200);
					}
				}
				wifi_state = wifi_state_idle;
				break;
			case wifi_state_socket_write:
			   printf("\r\n >>Writing data to client\r\n");
			   HAL_Delay(50);
			   wifi_state = wifi_state_idle;
			 break;
		   case wifi_state_idle:
			   /* For debug
			   idleCount++;
			   if(idleCount >= 100){
				printf(".");
				fflush(stdout);
				idleCount = 1;
			   }
			   */
			   wifiIdleTask();
			 break;
		   default:
			   break;
		 }
	}
}


/******** Wi-Fi Indication User Callback *********/
// Methods same for both modes: STA & AP
void ind_wifi_on()
{
  printf("\r\nAPind_wifi_on() WiFi Initialized and Ready..\r\n");
  wifi_state = wifi_state_ready;
}
void ind_wifi_ap_client_joined(uint8_t * client_mac_address)
{
  printf("\r\n>>Client joined... client MAC: %s\r\n", (const char*)client_mac_address);
  fflush(stdout);
  wifi_state = wifi_state_idle;
  power_KeepAlive();
}

void ind_wifi_ap_client_left(uint8_t * client_mac_address)
{
  printf("\r\n>>client left callback...\r\n");
  printf(">>client MAC address: ");
  printf((const char*)client_mac_address);
  fflush(stdout);
}

void ind_wifi_inputssi_callback(void)
{
  wifi_state = wifi_state_socket_write;
}

void ap_ind_wifi_connected()
{
  printf("\r\nAPind_wifi_connected()\r\n");
  wifi_state = wifi_state_connected;
}


void ind_wifi_socket_client_remote_server_closed(uint8_t *socketID, WiFi_Socket_t socket_type)
{
  uint8_t id = *socketID;
  printf("\r\n>>User Callback: remote server socket closed\r\n");
  printf("Socket ID closed: %d",id);//this will actually print the character/string, not the number
  fflush(stdout);
}

void ind_wifi_resuming()
{
  printf("\r\nwifi resuming from sleep user callback... \r\n");
  //Change the state to connect to socket if not connected
  wifi_state = wifi_state_socket;
}

void ind_socket_server_client_joined(void)
{
  printf("\r\nUser callback: Client joined...\r\n");
  fflush(stdout);
}

void ind_socket_server_client_left(void)
{
  printf("\r\nUser callback: Client left...\r\n");
  fflush(stdout);
}

/////////////////////////////////////////////////////////////////////

void COM_FromWifi(const void *ptr);

void ap_ind_wifi_socket_data_received(int8_t callback_server_id, int8_t socket_id,
		uint8_t * data_ptr, uint32_t message_size, uint32_t chunk_size, WiFi_Socket_t socket_type)
{
	//printf("APSocket ID: %d ServerId: %d type: %d\r\n",socket_id, callback_server_id, socket_type);
	memcpy(receivedCommandBuffer, data_ptr, 512);
	Frame* frame = (Frame*)receivedCommandBuffer;

/*
	//TODO In Access Point mode user can only detect device && write STA configuration
	if(frame->function == FRAME_SysCall || frame->function == FRAME_TestFrame ||
			(frame->function == FRAME_WriteData && frame->readWrite.structId == Structure_WiFiConfig)){
		COM_FromWifi(receivedCommandBuffer);
	}else{
		// Send back: illegal state

	} */
	COM_FromWifi(receivedCommandBuffer);
	wifi_state = wifi_state_idle;		// Do not REMOVE !!!!!!!
}

void ap_ind_wifi_error(WiFi_Status_t error_code)
{
  printf("\r\nAP ind_wifi_error %d 0x%x\r\n", error_code, error_code);
  if(error_code == WiFi_AT_CMD_RESP_ERROR)
  {
    wifi_state = wifi_state_idle;
    printf("\r\n WiFi Command Failed. \r\n User should now press the RESET Button(B2). \r\n");
  }
}


