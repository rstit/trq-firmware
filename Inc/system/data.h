#ifndef SYSTEM_DATA_H_
#define SYSTEM_DATA_H_


void data_Reload();
HAL_StatusTypeDef data_SaveWiFiConfig();
HAL_StatusTypeDef data_ClearWiFiConfig();
void data_Store();



#endif /* SYSTEM_DATA_H_ */
