/*
 * adc_measurement.c
 *
 *  Created on: 07.05.2018
 *      Author: jkonieczny
 */

#include <stdint.h>
#include <string.h>

#include "sdadc.h"
#include "spi.h"
#include "measurements.h"
#include "all.h"
#include "stm32f3xx_hal_tim.h"

extern SPI_HandleTypeDef hspi1;
extern SPI_HandleTypeDef hspi2;
extern SPI_HandleTypeDef hspi3;
extern DMA_HandleTypeDef hdma_spi1_rx;
extern DMA_HandleTypeDef hdma_spi1_tx;

extern SDADC_HandleTypeDef hsdadc1;
extern SDADC_HandleTypeDef hsdadc2;
extern SDADC_HandleTypeDef hsdadc3;
extern DMA_HandleTypeDef hdma_sdadc1;
extern DMA_HandleTypeDef hdma_sdadc2;
extern DMA_HandleTypeDef hdma_sdadc3;

extern TIM_HandleTypeDef htim7;

uint8_t* DDS_ConvertUint32toUint8Array(uint32_t value);
HAL_StatusTypeDef DDS_WriteRegister(const PRegister reg, void* data);
uint32_t DDS_CountFTWRegisterValue(uint32_t frequency);
uint8_t* DDS_ConvertUint32toUint8Array(uint32_t value);

struct {
	uint32_t delta;
	uint8_t maxItems;
	uint8_t started;
	uint8_t items;
	uint8_t got;
} adc_Status;

float lastAdcValues[3];

void ADC_SetTimer(float hz) {
	int base = 1125000;
	htim7.Init.Prescaler = 64;
	htim7.Init.Period = base / hz;
	if (HAL_TIM_Base_Init(&htim7) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}
}

HAL_StatusTypeDef ADC_Init() {
	DDS_Init();
	analogConfig.frequency = 4000000;
	// TODO: configure DDS and Attenuator
	HAL_StatusTypeDef result = HAL_OK;

	HAL_GPIO_WritePin(AFE_PWR_EN_GPIO_Port, AFE_PWR_EN_Pin, GPIO_PIN_RESET);

	uint32_t calib[] = {
			SDADC_CALIBRATION_SEQ_1,
			SDADC_CALIBRATION_SEQ_2,
			SDADC_CALIBRATION_SEQ_3
	};
	for (int i = 0; i < 3; i++) {
		result |= HAL_SDADC_CalibrationStart(&hsdadc1, calib[i]);
		result |= HAL_SDADC_PollForCalibEvent(&hsdadc1, HAL_MAX_DELAY);

		result |= HAL_SDADC_CalibrationStart(&hsdadc2, calib[i]);
		result |= HAL_SDADC_PollForCalibEvent(&hsdadc2, HAL_MAX_DELAY);

		result |= HAL_SDADC_CalibrationStart(&hsdadc3, calib[i]);
		result |= HAL_SDADC_PollForCalibEvent(&hsdadc3, HAL_MAX_DELAY);
	}
	adc_Status.maxItems = 20;
	adc_Status.started = 0;

	return result;
}

void setFrequency(uint32_t frequency) {
	frequency = DDS_CountFTWRegisterValue(frequency);
	uint8_t *frequencyData = DDS_ConvertUint32toUint8Array(frequency);
	DDS_WriteRegister(DDS_FTW, frequencyData);
}

HAL_StatusTypeDef ADC_Start() {
	if (adc_Status.started) {
		// TODO: why? how?
	}
//	f = 1000;
	HAL_GPIO_WritePin(AFE_PWR_EN_GPIO_Port, AFE_PWR_EN_Pin, GPIO_PIN_SET);

	DDS_Init();
	ATT_Set(0b110111);

	volatile uint32_t frequency = DDS_CountFTWRegisterValue(analogConfig.frequency);
	volatile uint8_t *frequencyData = DDS_ConvertUint32toUint8Array(frequency);
	DDS_WriteRegister(DDS_FTW, frequencyData);
	ADC_SetTimer(deviceConfig.adcFrequency);

	buffers_nextAdcBuffer();
	if (currentAdcBuffer) {
		currentAdcBuffer->timestamp = HAL_GetTick(); // osKernelSysTick();
		HAL_StatusTypeDef timStatus = HAL_TIM_Base_Start_IT(&htim7);
		if (timStatus != HAL_OK) {
			printf("[ADC] TIM start error\r\n");
		}
	    adc_Status.started = 1;
	    adc_Status.got = 0;
	    return HAL_OK;
	} else {
		printf("[ERROR] No free buffers\r\n");
		return HAL_ERROR;
	}
}

void ADC_Tim() {
	static volatile uint32_t last = 0;
	volatile uint32_t now = HAL_GetTick();
	HAL_StatusTypeDef status1 = HAL_SDADC_Start_IT(&hsdadc1);
	HAL_StatusTypeDef status2 = HAL_SDADC_Start_IT(&hsdadc2);
	HAL_StatusTypeDef status3 = HAL_SDADC_Start_IT(&hsdadc3);
	if (status1 != HAL_OK || status2 != HAL_OK || status3 != HAL_OK) {
		printf("[ADC] Can't start ADC conversions %d %d %d\r\n", status1, status2, status3);
	} else {
//		uint8_t item = 'T';
//		HAL_UART_Transmit(&huart1, &item, 1, 10);
	}
	last = now;
}


HAL_StatusTypeDef ADC_Stop() {
    HAL_TIM_Base_Stop_IT(&htim7);
	HAL_SDADC_Stop_IT(&hsdadc1);
	HAL_SDADC_Stop_IT(&hsdadc2);
	HAL_SDADC_Stop_IT(&hsdadc3);
	adc_Status.started = 0;
	printf("[ADC] Finalize with %d items\r\n", adc_Status.items);
	buffers_finalizeBuffer((MeasurementFrame*) currentAdcBuffer,
			adc_Status.items);

	frame_send(currentAdcBuffer, sizeof(currentAdcBuffer));
	HAL_GPIO_WritePin(AFE_PWR_EN_GPIO_Port, AFE_PWR_EN_Pin, GPIO_PIN_RESET);
	return HAL_OK;
}

void ADC_ChannelReady() {
	printf("[ADC] Add frame\r\n");
	if (currentAdcBuffer == 0) {
		printf("[ADC] No free buffers!\r\n");
		return;
	}
	buffers_finalizeBuffer((MeasurementFrame*) currentAdcBuffer,
			adc_Status.items);

	currentAdcBuffer->delta = lround(1000/deviceConfig.adcFrequency);
	frame_send(currentAdcBuffer, currentAdcBuffer->frameSize);
	adc_Status.items = 0;
	buffers_nextAdcBuffer();
	if (currentAdcBuffer == 0) {
		printf("[ADC] No free buffers!\r\n");
	}
}

// IRQ handlers


/**
 * @brief Read and store SDADC conversion result
 */
void HAL_SDADC_ConvCpltCallback(SDADC_HandleTypeDef* hsdadc) {
	if (adc_Status.started != 1) {
		// ignore interrupt if adc measurements not started
		return;
	}
	int16_t value = HAL_SDADC_GetValue(hsdadc) & 0xffff;
	volatile uint8_t whichOne;
	if (hsdadc == &hsdadc1) {
		whichOne = 0;
	} else if (hsdadc == &hsdadc2) {
		whichOne = 1;
	} else if (hsdadc == &hsdadc3) {
		whichOne = 2;
	} else {
		return;
	}

	if (adc_Status.got & (1 << whichOne)) {
		printf("[ADC] IRQ misaligned\r\n");
		adc_Status.got = 0;
		return;
	}
	adc_Status.got |= 1 << whichOne;
 	lastAdcValues[whichOne] = (value / 32767.0f) * 3.3f / 2.0f;

	currentAdcBuffer->values[adc_Status.items][whichOne] = value;

	if (adc_Status.got == 0b111) {
		adc_Status.got = 0;
		adc_Status.items++;
//		char buf[64];
//		sprintf(buf, "%1.3f,%1.3f,%1.3f\r\n", lastAdcValues[0], lastAdcValues[1], lastAdcValues[2]);
//		HAL_UART_Transmit(&huart1, buf, strlen(buf), 10);
		if (adc_Status.items % 20 == 0) {
			ADC_ChannelReady();
			adc_Status.items = 0;
//			uint8_t item = 'D';
//			HAL_UART_Transmit(&huart1, &item, 1, 1);
		}
	}
}
