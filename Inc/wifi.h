
#ifndef WIFI_H_
#define WIFI_H_

#include "wifi_interface.h"
#include "wifi_globals.h"
#include "wifi_module.h"
#include "system/data.h"
#include <stm32_spwf_wifi.h>


void wifiIdleTask();

#define WIFI_DEBUG	0
#if WIFI_DEBUG > 0

#define WIFI_DBG(format, ...) printf(format, ##__VA_ARGS__)

#endif

extern char *WIFI_TCPServer_Protocol;
extern uint32_t WIFI_TCPServer_PortNumber;

typedef enum {
	wifi_state_reset = 0,
	wifi_state_ready,
	wifi_state_idle,
	wifi_state_connected,
	wifi_state_connecting,
	wifi_state_socket,
	wifi_state_socket_write,
	wifi_state_disconnected,
	wifi_state_activity,
	wifi_state_inter,
	wifi_state_print_data,
	wifi_state_error,
	wifi_undefine_state = 0xFF,
} wifi_state_t;

typedef enum{
	WIFI_SUCCESS = 0,
	WIFI_CONFIG_ERR,
	WIFI_AP_SETTINGS_ERR,
	WIFI_START_ERR

}WIFI_Result_t;

typedef struct{
	void (*data_received)(int8_t callback_server_id, int8_t socket_id,
			uint8_t * data_ptr, uint32_t message_size, uint32_t chunk_size, WiFi_Socket_t socket_type);
	void (*error)(WiFi_Status_t error_code);
	void (*connected)(void);

}WIFI_Callbacks_t;

WIFI_Callbacks_t WIFI_Callbacks;


void WIFI_PrintStatus(WiFi_Status_t status);
WiFi_Status_t WIFI_GetAllConfiguration();

/* AT&V<cr> All possible cnfigurations
 *
# Dumping All Configuration Keys:
#  nv_manuf = STM
#  nv_model = SPWF01SA11
#  nv_serial = 1317D01827
#  nv_wifi_macaddr = 00:80:E1:BE:58:26
#  etf_mode = 0
#  blink_led = 0
#  wind_off_low = 0x00000000
#  wind_off_medium = 0x00000000
#  wind_off_high = 0x00000000
#  user_desc = anonymous
#  escape_seq = at+s.
#  localecho1 = 0
#  console1]speed = 115200
#  console1_hwfc = 1
#  console1_enabled = 1
#  console1_delimiter = 0x0000002C
#  console1_errs = 1
#  sleep_enabled = 0
#  standby_enabled = 0
#  standby_time = 10
#  wifi_tx_msdu_lifetime = 0
#  wifi_rx_msdu_lifetime = 0
#  wifi_operational_mode = 0x00000011
#  wifi_beacon_wakeup = 1
#  wifi_beacon_interval = 100
#  wifi_listen_interval = 0
#  wifi_rts_threshold = 3000
#  wifi_ssid = 41:63:68:69:6C:6C:65:73:5F:45:31:42:45:35:38:32:36:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00
#  wifi_ssid_len = 17
#  wifi_txfail_thresh = 5
#  wifi_dtim_period = 1
#  wifi_add_tim_ie = 1
#  wifi_ht_mode = 0
#  wifi_channelnum = 6
#  wifi_opr_rate_mask = 0x00003FCF
#  wifi_bas_rate_mask = 0x0000000F
#  wifi_mode = 3
#  wifi_region = 1
#  wifi_auth_type = 0
#  wifi_atim_window = 0
#  wifi_powersave = 0
#  wifi_tx_power = 12
#  wifi_rssi_thresh = 0
#  wifi_rssi_hyst = 0
#  wifi_ap_idle_timeout = 120
#  wifi_beacon_loss_thresh = 10
#  wifi_priv_mode = 0
#  wifi_wep_keys[0] = 00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00
#  wifi_wep_keys[1] = 00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00
#  wifi_wep_keys[2] = 00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00
#  wifi_wep_keys[3] = 00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00
#  wifi_wep_key_lens = 00:00:00:00
#  wifi_wep_default_key = 0
#  wifi_wpa_psk_raw = 00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00
#  wifi_wpa_psk_text = h1234567
#  ip_use_dhcp = 1
#  ip_use_httpd = 0
#  ip_mtu = 1500
#  ip_hostname = AchillesBoard
#  ip_apdomainname = achilles.net
#  ip_apredirect = test.html
#  ip_ipaddr = 192.168.0.50
#  ip_netmask = 255.255.255.0
#  ip_gw = 192.168.0.1
#  ip_dns = 192.168.0.1
#  ip_http_get_recv_timeout = 3000
#  ip_wait_timeout = 12000
#  ip_dhcp_timeout = 20
#  ip_sockd_timeout = 250
#  ip_dhcp_lease_time = 120
#  ip_dns_mode = 0
#  ip_use_cgis = 0x0000000F
#  ip_use_ssis = 0x0000000F
#  ip_use_decoder = 0x00000000
#  ip_block_pings = 0

*/

WiFi_Status_t WIFI_socket_server_open(uint32_t port_number);

// New API
void WIFI_PowerUp();
void WIFI_PowerDown();
void WIFI_Reset();
void WIFI_LED_OnOff(int offOn);
void WIFI_LED_Toggle();
void WIFI_RestoreDefaultSettingsByPinState();

WiFi_Status_t WIFI_Module_Init(uint32_t uart_speed);
WiFi_Status_t WIFI_SetConfigurationValue(const char* name, uint32_t value);
WiFi_Status_t WIFI_SetConfigurationAddress(const char* name, const char* address);

//WiFi_Status_t WIFI_GetConfigurationValue(const char* name);

WiFi_Status_t WIFI_ExecuteCommand(const char* command);
WiFi_Status_t WIFI_ExecuteCommandWithArgInt(const char* format, uint32_t value);
WiFi_Status_t WIFI_ExecuteCommandWithArgString(const char* format, const char* value);

void WIFI_LED_BlinkOK(uint8_t blinksNumer, uint32_t blinkPeriod);
void WIFI_LED_BlinkFault(uint8_t blinksNumer, uint32_t blinkPeriod);



#endif /* WIFI_H_ */
