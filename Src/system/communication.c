/*
 * communication.c
 *
 *  Created on: 13.05.2018
 *      Author: jkonieczny
 */
#include <math.h>

#include "structures.h"

#include "stm32f3xx.h"
#include "stm32f3xx_hal.h"
#include "usart.h"

#include "wifi.h"
#include "wifi_ap.h"
#include "periphs/all.h"

#define FRAME_RESPONSES 5

/**
 * @brief Static allocated frame buffers to send
 */
typedef union FrameResponse {
	Frame frame;
	uint8_t data[128];
} FrameResponse;
FrameResponse responses[FRAME_RESPONSES];

extern WiFiConfig wifiConfig;	// Configuration saved in FLASH

void frame_send(Frame *frame, int size);

SysCallResponseFrame frameResponseSysCall;
WriteDataResponseFrame writeDataResponse;
static void COM_SendUnknowFrameResponse();

TestFrame testFrameResponse;
static void COM_SendTestFrame(uint16_t frameDataSize, COM_Tx responseTo);

enum Structs {
	POWER_STATUS = 0x02,
	ANALOG_CONFIG = 0x03,
	DEVICE_CONFIG = 0x05
};

void frame_ReadData(const Frame *frame) {
	FrameResponse *response = 0;
	for (int i = 0; i < FRAME_RESPONSES; i++) {
		if (responses[i].frame.function == 0) {
			response = &responses[i];
			break;
		}
	}
	if (response == 0) {
		return;
	}

	response->frame.function = 0x81;
	response->frame.structId = frame->readWrite.structId;

	switch (frame->readWrite.structId) {
	case POWER_STATUS:
		updateBatteryState();
		response->frame.readWrite.structSize = sizeof(batteryStatus);
		memcpy(response->frame.readWrite.structData, &batteryStatus,
				sizeof(batteryStatus));
		break;
	case ANALOG_CONFIG:
		response->frame.readWrite.structSize = sizeof(analogConfig);
		memcpy(response->frame.readWrite.structData, &analogConfig,
				sizeof(analogConfig));
		if (deviceStatus.state == SCAN) {
			setFrequency(analogConfig.frequency);
		}
		break;
	case DEVICE_CONFIG:
		response->frame.readWrite.structSize = sizeof(deviceConfig);
		memcpy(response->frame.readWrite.structData, &deviceConfig,
				sizeof(analogConfig));
		break;
	default:
		response->frame.frameSize = 0;
	}
	response->frame.frameSize = 3 + 4 + response->frame.readWrite.structSize;
	frame_send(&response->frame, response->frame.frameSize);
}

void frame_WriteData(const Frame *frame) {
	FrameResponse *response = 0;
	for (int i = 0; i < FRAME_RESPONSES; i++) {
		if (responses[i].frame.function == 0) {
			response = &responses[i];
			break;
		}
	}
	if (response == 0) {
		// TODO: notify error
		return;
	}
	response->frame.function = 0x82;
	response->frame.structId = frame->readWrite.structId;
	response->frame.writeResponse.status = 1;
	response->frame.frameSize = 3 + 2;

	switch (frame->readWrite.structId) {
	case POWER_STATUS:
		// not allowed, read only
		break;
	case ANALOG_CONFIG: {
		response->frame.writeResponse.status = 0;
		int size = sizeof(analogConfig);
		memcpy(&analogConfig, frame->readWrite.structData, size);
		break;
	}
	case Structure_WiFiConfig:
#ifdef DEBUG_PRINT
		printf("\r\nFrame to configuration STA \"%s\" \"%s\" \"%d\"\r\n",
				frame->wifiConfig.ssid, frame->wifiConfig.password,
				frame->wifiConfig.flags);
#endif
		strncpy(wifiConfig.ssid, frame->wifiConfig.ssid,
				sizeof(wifiConfig.ssid));
		strncpy(wifiConfig.password, frame->wifiConfig.password,
				sizeof(wifiConfig.password));
		wifiConfig.flags = frame->wifiConfig.flags;
		data_SaveWiFiConfig();

		memset(&writeDataResponse, 0x00, sizeof(writeDataResponse));
		writeDataResponse.id = FRAME_WriteDataResponse;
		writeDataResponse.frameSize = sizeof(writeDataResponse);

		if ((frame->wifiConfig.flags & WIFI_FLAGS_MASK_PRIV_MODE) == WEP
				&& strlen(frame->wifiConfig.password) != 13) { // 13-characters required by WEP 128-bit
			writeDataResponse.status = WifiConfigResponseEncryptionError;
		} else if ((frame->wifiConfig.flags & WIFI_FLAGS_MASK_PRIV_MODE)
				== WPA_Personal && strlen(frame->wifiConfig.password) > 32) {
			writeDataResponse.status = WifiConfigResponseEncryptionError;
		} else {
			writeDataResponse.status = WifiConfigResponseOK;
		}
		frame_send((Frame*) &writeDataResponse, sizeof(writeDataResponse));
		deviceStatus.requestRestart = HAL_GetTick() + 2000;
		/*
		 // Check if network is available and WiFi encryption is match
		 status = WIFI_CheckConnectionParams(frame->wifiConfig.ssid, frame->wifiConfig.flags & WIFI_FLAGS_MASK_PRIV_MODE);
		 memset(&writeDataResponse, 0x00, sizeof(writeDataResponse));
		 writeDataResponse.id = FRAME_WriteDataResponse;
		 writeDataResponse.frameSize = sizeof(writeDataResponse);

		 if(status == WiFi_MODULE_SUCCESS){
		 // If OK, then:
		 // 1. Save data to Flash
		 strncpy(wifiConfig.ssid, frame->wifiConfig.ssid, sizeof(wifiConfig.ssid));
		 strncpy(wifiConfig.password, frame->wifiConfig.password, sizeof(wifiConfig.password));
		 wifiConfig.flags = frame->wifiConfig.flags;
		 data_SaveWiFiConfig();
		 // 2. Send response OK
		 writeDataResponse.status = WifiConfigResponseOK;
		 //responseTo((uint8_t*)&writeDataResponse, writeDataResponse.frameSize);
		 // 3. Restart board
		 NVIC_SystemReset();
		 }else if(status == WiFi_SSID_ERROR){
		 //Send response: Not FOUND
		 writeDataResponse.status = WifiConfigResponseNetworkNotFound;
		 responseTo((uint8_t*)&writeDataResponse, writeDataResponse.frameSize);
		 }else if(status == WiFi_SecKey_ERROR){
		 // Send response: Encryption failed
		 writeDataResponse.status = WifiConfigResponseEncryptionError;
		 responseTo((uint8_t*)&writeDataResponse, writeDataResponse.frameSize);
		 }else{
		 writeDataResponse.status = WifiConfigModuleError;
		 responseTo((uint8_t*)&writeDataResponse, writeDataResponse.frameSize);
		 }
		 */
		break;
	default:
		return;
	}
	frame_send(&response->frame, response->frame.frameSize);
}

void frame_SysCall(Frame *frame) {
	switch (frame->sysCallFunction) {
	case SysCall_Beep:
		memset(&frameResponseSysCall, 0x00, sizeof(frameResponseSysCall));
		frameResponseSysCall.frameId = FRAME_SysCallResponse;
		frameResponseSysCall.frameSize = sizeof(frameResponseSysCall);
		frameResponseSysCall.responseCode = SysCall_ResponseOK;
		frame_send((Frame*) &frameResponseSysCall,
				sizeof(frameResponseSysCall));
		break;
	case SysCall_BeepLEDs:
		memset(&frameResponseSysCall, 0x00, sizeof(frameResponseSysCall));
		frameResponseSysCall.frameId = FRAME_SysCallResponse;
		frameResponseSysCall.frameSize = sizeof(frameResponseSysCall);
		frameResponseSysCall.responseCode = SysCall_ResponseOK;
		frame_send((Frame*) &frameResponseSysCall,
				sizeof(frameResponseSysCall));
		//How to make beep without speaker? ... blink LED
		WIFI_LED_BlinkOK(5, 200);
		break;
	case SysCall_Reset:
		memset(&frameResponseSysCall, 0x00, sizeof(frameResponseSysCall));
		frameResponseSysCall.frameId = FRAME_SysCallResponse;
		frameResponseSysCall.frameSize = sizeof(frameResponseSysCall);
		frameResponseSysCall.responseCode = SysCall_ResponseOK;
		frame_send((Frame*) &frameResponseSysCall,
				sizeof(frameResponseSysCall));
		WIFI_LED_BlinkOK(1, 200);
		WIFI_LED_BlinkFault(1, 200);
		WIFI_LED_BlinkOK(1, 200);
		// Restart board
		NVIC_SystemReset();
		break;
	default:
		COM_SendUnknowFrameResponse(frame_send);
		break;
	}
}

/**
 * @brief parse and response to packet
 *
 * @param frame Pointer to frame
 * @param responseTo function which should be used to answer
 */
void COM_parse(const Frame *frame, COM_Tx responseTo) {
	void (*localResponseTo)(Frame *frame, int size) = frame_send;

	switch (frame->function) {
	case FRAME_ReadData:
		frame_ReadData(frame);
		break;
	case FRAME_WriteData:
		frame_WriteData(frame);
		break;
	case FRAME_SysCall:
		frame_SysCall(frame);
		break;
	case FRAME_TestFrame:
		// Send response with given size
		COM_SendTestFrame(frame->testFrame.responseSize, localResponseTo);
		break;
	default:
		COM_SendUnknowFrameResponse(localResponseTo);
		break;
	}
}

static void COM_SendUnknowFrameResponse(COM_Tx responseTo) {
	memset(&frameResponseSysCall, 0x00, sizeof(frameResponseSysCall));
	frameResponseSysCall.frameId = FRAME_SysCallResponse;
	frameResponseSysCall.frameSize = sizeof(frameResponseSysCall);
	frameResponseSysCall.responseCode = SysCall_ResponseUnknownFrameType;
	responseTo((Frame*) &frameResponseSysCall, sizeof(frameResponseSysCall));
}

static void COM_SendTestFrame(uint16_t frameDataSize, COM_Tx responseTo){
	testFrameResponse.frameId = FRAME_TestFrameResponse;
	testFrameResponse.frameSize = 1 + 2 + frameDataSize;
	memset(&testFrameResponse.frameData, 0x00, frameDataSize);
	testFrameResponse.frameData[0] = 0x3F; // mark start data frame
	testFrameResponse.frameData[frameDataSize - 1] = 0xF3; // mark end data frame
	responseTo((Frame*) &testFrameResponse, testFrameResponse.frameSize);
}



int WiFi_response(void *ptr, int length) {
	wifi_socket_server_write(length, ptr);
	return 0;
}

void COM_FromWifi(const void *ptr) {
	COM_Tx tx = WiFi_response;
	COM_parse((Frame*) (ptr), tx);
}

int Wire_response(void *ptr, int length) {
	return HAL_UART_Transmit(&huart1, ptr, length, 10) == HAL_OK;
}

void COM_FromWires(const void *ptr) {
	COM_Tx tx = Wire_response;
	COM_parse((Frame*) (ptr), tx);
}


typedef union Buffer {
	uint8_t buffer[512];
	Frame frame;
} Buffer;

Buffer wire;

uint32_t WireLastRx = 0;
uint16_t WirePosition = 0;
/**
* @brief This function handles USART2 global interrupt / USART2 wake-up interrupt through EXTI line 26.
*/
void USART1_IRQHandler(void) {
	uint32_t now = HAL_GetTick();
	int diff = now - WireLastRx;

	if (diff > 100) {
//		printf("diff = %d\r\n", diff);
		WirePosition = 0;
	}

	if (USART1->ISR & USART_ISR_RXNE) {
		wire.buffer[WirePosition++] = USART1->RDR;
		WireLastRx = now;
//		printf("U[%d]:%02X\r\n", WirePosition-1, wire.buffer[WirePosition-1]);
		if (WirePosition >= 500) {
			WirePosition = 0;
		}
		if (WirePosition >= 2 && WirePosition == wire.frame.frameSize) {
			COM_FromWires(wire.buffer);
			WirePosition = 0;
		}
	}
	if (USART1->ISR & USART_ISR_ORE) {
		USART1->ICR |= USART_ICR_ORECF;
		volatile c = USART1->RDR;
//		printf("ORE\r\n", diff);
	}
	USART1->ICR |= USART_ICR_ORECF;
}
