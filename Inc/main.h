/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/
#define HSE_VALUE 16000000
#define TIM12_CH1_PRESCALER HSE_VALUE/1000
#define TIM12_CH1_PERIOD TIM12_CH1_PRESCALER/16
#define TIM12_CH1_PULSE TIM12_CH1_PERIOD/2

#define S_XTAL_IN_Pin GPIO_PIN_14
#define S_XTAL_IN_GPIO_Port GPIOC
#define S_XTAL_OUT_Pin GPIO_PIN_15
#define S_XTAL_OUT_GPIO_Port GPIOC
#define P_XTAL_IN_Pin GPIO_PIN_0
#define P_XTAL_IN_GPIO_Port GPIOF
#define P_XTAL_OUT_Pin GPIO_PIN_1
#define P_XTAL_OUT_GPIO_Port GPIOF
#define I2C_nIRQ_ACC1_Pin GPIO_PIN_0
#define I2C_nIRQ_ACC1_GPIO_Port GPIOC
#define I2C_nIRQ_ACC1_EXTI_IRQn EXTI0_IRQn
#define I2C_nIRQ_ACC2_Pin GPIO_PIN_1
#define I2C_nIRQ_ACC2_GPIO_Port GPIOC
#define I2C_nIRQ_ACC2_EXTI_IRQn EXTI1_IRQn
#define DDS_SPI_MISO_Pin GPIO_PIN_2
#define DDS_SPI_MISO_GPIO_Port GPIOC
#define DDS_SPI_MOSI_Pin GPIO_PIN_3
#define DDS_SPI_MOSI_GPIO_Port GPIOC
#define WLAN_CTS_Pin GPIO_PIN_0
#define WLAN_CTS_GPIO_Port GPIOA
#define WLAN_RTS_Pin GPIO_PIN_1
#define WLAN_RTS_GPIO_Port GPIOA
#define WLAN_TX_Pin GPIO_PIN_2
#define WLAN_TX_GPIO_Port GPIOA
#define WLAN_RX_Pin GPIO_PIN_3
#define WLAN_RX_GPIO_Port GPIOA
#define N411_Pin GPIO_PIN_4
#define N411_GPIO_Port GPIOF
#define MEM_SPI_NSS_Pin GPIO_PIN_4
#define MEM_SPI_NSS_GPIO_Port GPIOA
#define MEM_SPI_SCK_Pin GPIO_PIN_5
#define MEM_SPI_SCK_GPIO_Port GPIOA
#define MEM_SPI_MISO_Pin GPIO_PIN_6
#define MEM_SPI_MISO_GPIO_Port GPIOA
#define MEM_SPI_MOSI_Pin GPIO_PIN_7
#define MEM_SPI_MOSI_GPIO_Port GPIOA
#define MEM_nPWR_EN_Pin GPIO_PIN_4
#define MEM_nPWR_EN_GPIO_Port GPIOC
#define WLAN_nRESET_Pin GPIO_PIN_5
#define WLAN_nRESET_GPIO_Port GPIOC
#define WLAN_BOOT_Pin GPIO_PIN_0
#define WLAN_BOOT_GPIO_Port GPIOB
#define WLAN_MODE_Pin GPIO_PIN_1
#define WLAN_MODE_GPIO_Port GPIOB
#define WLAN_RESTORE_Pin GPIO_PIN_2
#define WLAN_RESTORE_GPIO_Port GPIOB
#define WLAN_nPWR_EN_Pin GPIO_PIN_7
#define WLAN_nPWR_EN_GPIO_Port GPIOE
#define N416_Pin GPIO_PIN_10
#define N416_GPIO_Port GPIOE
#define N417_Pin GPIO_PIN_11
#define N417_GPIO_Port GPIOE
#define AFE_RX1_P_Pin GPIO_PIN_12
#define AFE_RX1_P_GPIO_Port GPIOE
#define AFE_RX1_N_Pin GPIO_PIN_13
#define AFE_RX1_N_GPIO_Port GPIOE
#define AFE_RX2_P_Pin GPIO_PIN_15
#define AFE_RX2_P_GPIO_Port GPIOE
#define AFE_RX2_N_Pin GPIO_PIN_10
#define AFE_RX2_N_GPIO_Port GPIOB
#define USER_AUDIO_DRV_Pin GPIO_PIN_14
#define USER_AUDIO_DRV_GPIO_Port GPIOB
#define USB_DETECT_Pin GPIO_PIN_15
#define USB_DETECT_GPIO_Port GPIOB
#define USB_DETECT_EXTI_IRQn EXTI15_10_IRQn
#define DDS_SPI_SCLK_Pin GPIO_PIN_8
#define DDS_SPI_SCLK_GPIO_Port GPIOD
#define USER_LED_BAT_Pin GPIO_PIN_9
#define USER_LED_BAT_GPIO_Port GPIOD
#define USER_LED_WIFI_Pin GPIO_PIN_10
#define USER_LED_WIFI_GPIO_Port GPIOD
#define USER_LED_SCAN_Pin GPIO_PIN_11
#define USER_LED_SCAN_GPIO_Port GPIOD
#define USER_LED_FAULT_Pin GPIO_PIN_12
#define USER_LED_FAULT_GPIO_Port GPIOD
#define USER_SW_nPOWER_Pin GPIO_PIN_13
#define USER_SW_nPOWER_GPIO_Port GPIOD
#define AFE_RX3_P_Pin GPIO_PIN_14
#define AFE_RX3_P_GPIO_Port GPIOD
#define AFE_RX3_N_Pin GPIO_PIN_15
#define AFE_RX3_N_GPIO_Port GPIOD
#define DDS_UPDATE_Pin GPIO_PIN_6
#define DDS_UPDATE_GPIO_Port GPIOC
#define DDS_RESET_Pin GPIO_PIN_7
#define DDS_RESET_GPIO_Port GPIOC
#define DDS_PWR_EN_Pin GPIO_PIN_8
#define DDS_PWR_EN_GPIO_Port GPIOC
#define AFE_PWR_EN_Pin GPIO_PIN_9
#define AFE_PWR_EN_GPIO_Port GPIOC
#define SYS_CLK_Pin GPIO_PIN_8
#define SYS_CLK_GPIO_Port GPIOA
#define DBG_UART_TX_Pin GPIO_PIN_9
#define DBG_UART_TX_GPIO_Port GPIOA
#define DBG_UART_RX_Pin GPIO_PIN_10
#define DBG_UART_RX_GPIO_Port GPIOA
#define USB_D_N_Pin GPIO_PIN_11
#define USB_D_N_GPIO_Port GPIOA
#define USB_D_P_Pin GPIO_PIN_12
#define USB_D_P_GPIO_Port GPIOA
#define JTAG_TMS_Pin GPIO_PIN_13
#define JTAG_TMS_GPIO_Port GPIOA
#define USB_ATTACH_Pin GPIO_PIN_6
#define USB_ATTACH_GPIO_Port GPIOF
#define JTAG_TCK_Pin GPIO_PIN_14
#define JTAG_TCK_GPIO_Port GPIOA
#define JTAG_TDI_Pin GPIO_PIN_15
#define JTAG_TDI_GPIO_Port GPIOA
#define CHARGER_EN1_Pin GPIO_PIN_0
#define CHARGER_EN1_GPIO_Port GPIOD
#define CHARGER_EN2_Pin GPIO_PIN_1
#define CHARGER_EN2_GPIO_Port GPIOD
#define CHARGER_CE_Pin GPIO_PIN_2
#define CHARGER_CE_GPIO_Port GPIOD
#define CHARGER_PG_Pin GPIO_PIN_3
#define CHARGER_PG_GPIO_Port GPIOD
#define CHARGER_CHG_Pin GPIO_PIN_4
#define CHARGER_CHG_GPIO_Port GPIOD
#define SPI3_nCS_Pin GPIO_PIN_5
#define SPI3_nCS_GPIO_Port GPIOD
#define USER_SW_nSCAN_Pin GPIO_PIN_6
#define USER_SW_nSCAN_GPIO_Port GPIOD
#define USER_SW_nSCAN_EXTI_IRQn EXTI9_5_IRQn
#define USER_SW_nSPARE_Pin GPIO_PIN_7
#define USER_SW_nSPARE_GPIO_Port GPIOD
#define JTAG_TDO_Pin GPIO_PIN_3
#define JTAG_TDO_GPIO_Port GPIOB
#define JTAG_nTRST_Pin GPIO_PIN_4
#define JTAG_nTRST_GPIO_Port GPIOB
#define I2C_nIRQ_BAT_Pin GPIO_PIN_5
#define I2C_nIRQ_BAT_GPIO_Port GPIOB
#define I2C_nIRQ_BAT_EXTI_IRQn EXTI9_5_IRQn
#define I2C_SCL_Pin GPIO_PIN_6
#define I2C_SCL_GPIO_Port GPIOB
#define I2C_SDA_Pin GPIO_PIN_7
#define I2C_SDA_GPIO_Port GPIOB
#define N414_Pin GPIO_PIN_8
#define N414_GPIO_Port GPIOB
#define DDS_SPI_nCS_Pin GPIO_PIN_9
#define DDS_SPI_nCS_GPIO_Port GPIOB
#define PWR_KEEP_Pin GPIO_PIN_0
#define PWR_KEEP_GPIO_Port GPIOE

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
 #define USE_FULL_ASSERT    1U 

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
