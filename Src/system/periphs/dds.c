/*
 * dds.c
 *
 *  Created on: 10.05.2018
 *      Author: jkonieczny
 */

#include <stdint.h>
#include <spi.h>
#include <gpio.h>
#include "all.h"
#include <math.h>

/**
 * Protocol description (most important):
 *
 * 1. Frame
 *
 * bits:
 *   R/^W:1 (1-read, 0-write)
 *   X
 *   X
 *   func:5 – function number
 *
 */

#define fSYSCLK	100000000.0000f  //[100MHz] = 100*10^6
#define ex2To32	4294967296.0f // 2^32

extern SPI_HandleTypeDef hspi2;
//PB9 – Chip Select

#define Select() HAL_GPIO_WritePin(DDS_SPI_nCS_GPIO_Port, DDS_SPI_nCS_Pin, GPIO_PIN_RESET)
#define Deselect() HAL_GPIO_WritePin(DDS_SPI_nCS_GPIO_Port, DDS_SPI_nCS_Pin, GPIO_PIN_SET)

void DDS_Init () {
	// Power Down
	HAL_GPIO_WritePin(DDS_PWR_EN_GPIO_Port, DDS_PWR_EN_Pin, GPIO_PIN_RESET);
	HAL_Delay(50);
	// Reset set to High
	HAL_GPIO_WritePin(DDS_RESET_GPIO_Port, DDS_RESET_Pin, GPIO_PIN_SET);
	HAL_Delay(50);
	// Power Up
	HAL_GPIO_WritePin(DDS_PWR_EN_GPIO_Port, DDS_PWR_EN_Pin, GPIO_PIN_SET);
	HAL_Delay(50);
	// Finish reset state
	HAL_GPIO_WritePin(DDS_RESET_GPIO_Port, DDS_RESET_Pin, GPIO_PIN_RESET);
	HAL_Delay(50);
}

void DDS_PowerOn () {
	// Power Down
	HAL_GPIO_WritePin(DDS_PWR_EN_GPIO_Port, DDS_PWR_EN_Pin, GPIO_PIN_RESET);
	HAL_Delay(50);
	// Reset set to High
	HAL_GPIO_WritePin(DDS_RESET_GPIO_Port, DDS_RESET_Pin, GPIO_PIN_SET);
	HAL_Delay(50);
	// Power Up
	HAL_GPIO_WritePin(DDS_PWR_EN_GPIO_Port, DDS_PWR_EN_Pin, GPIO_PIN_SET);
	HAL_Delay(100);
	// Finish reset state
	HAL_GPIO_WritePin(DDS_RESET_GPIO_Port, DDS_RESET_Pin, GPIO_PIN_RESET);
	HAL_Delay(100);
}

void DDS_PowerOff () {
	// Power Down
	HAL_GPIO_WritePin(DDS_PWR_EN_GPIO_Port, DDS_PWR_EN_Pin, GPIO_PIN_RESET);
	HAL_Delay(50);
	// Reset set to High
	HAL_GPIO_WritePin(DDS_RESET_GPIO_Port, DDS_RESET_Pin, GPIO_PIN_SET);
	HAL_Delay(50);
	// Power Up
	HAL_GPIO_WritePin(DDS_PWR_EN_GPIO_Port, DDS_PWR_EN_Pin, GPIO_PIN_SET);
	HAL_Delay(100);
	// Finish reset state
	HAL_GPIO_WritePin(DDS_RESET_GPIO_Port, DDS_RESET_Pin, GPIO_PIN_RESET);
	HAL_Delay(100);
}

HAL_StatusTypeDef DDS_WriteRegister(const PRegister reg, void* data) {
	HAL_GPIO_WritePin(DDS_UPDATE_GPIO_Port, DDS_UPDATE_Pin, GPIO_PIN_RESET);
	Select();
	HAL_StatusTypeDef status = HAL_OK;
	uint8_t instr = reg.address & 0x7f;
	status |= HAL_SPI_Transmit(&hspi2, &instr, 1, 20);

	HAL_GPIO_WritePin(DDS_UPDATE_GPIO_Port, DDS_UPDATE_Pin, GPIO_PIN_SET);
	HAL_Delay(2);
	HAL_GPIO_WritePin(DDS_UPDATE_GPIO_Port, DDS_UPDATE_Pin, GPIO_PIN_RESET);

	status |= HAL_SPI_Transmit(&hspi2, data, reg.length, 200);

	HAL_GPIO_WritePin(DDS_UPDATE_GPIO_Port, DDS_UPDATE_Pin, GPIO_PIN_SET);
	HAL_Delay(5);
	HAL_GPIO_WritePin(DDS_UPDATE_GPIO_Port, DDS_UPDATE_Pin, GPIO_PIN_RESET);
	Deselect();
	HAL_Delay(2);
	return status;
}

HAL_StatusTypeDef DDS_ReadRegister(const PRegister reg, void* data) {
	Select();
	HAL_StatusTypeDef result = HAL_ERROR;
	uint8_t instr = reg.address | 0x80;
	result = HAL_SPI_Transmit(&hspi2, &instr, 1, 10);
	if (result != HAL_OK) {
		Deselect();
		return result;
	}
	result = HAL_SPI_Receive(&hspi2, data, reg.length, 100);
	Deselect();
	return result;
}

uint32_t DDS_CountFTWRegisterValue(uint32_t frequency){
	return (uint32_t)round(ex2To32*(frequency/fSYSCLK));
}

uint8_t* DDS_ConvertUint32toUint8Array(uint32_t value){
	static uint8_t resultUint32[4]= {0, 0, 0, 0};

	resultUint32[3] = (value & 0x000000ff);
	resultUint32[2] = (value & 0x0000ff00) >> 8;
	resultUint32[1] = (value & 0x00ff0000) >> 16;
	resultUint32[0] = (value & 0xff000000) >> 24;

	return resultUint32;
}

void DDS_DumpRegisters(){
#ifdef DEBUG_PRINT
	HAL_StatusTypeDef result = HAL_OK;

	printf("=-> DDS ReadRegisters:\n");
	uint8_t dataCFR1[] = {0, 0, 0, 0};
	result |= DDS_ReadRegister(DDS_CFR1, dataCFR1);
	printf("|-> DDS CFR1: [%d] 0x%x, 0x%x 0x%x 0x%x\n", result, dataCFR1[0], dataCFR1[1], dataCFR1[2], dataCFR1[3]);

	uint8_t dataCFR2[] = {0, 0};
	result |= DDS_ReadRegister(DDS_CFR2, dataCFR2);
	printf("|-> DDS CFR2: [%d] 0x%x, 0x%x\n", result, dataCFR2[0], dataCFR2[1]);

	uint8_t dataDAC[] = {0, 0, 0, 0};
	result |= DDS_ReadRegister(DDS_DAC, dataDAC);
	printf("|-> DDS DAC : [%d] 0x%x, 0x%x 0x%x 0x%x\n", result, dataCFR1[0], dataCFR1[1], dataCFR1[2], dataCFR1[3]);

	uint8_t dataFTW[] = {0, 0, 0, 0};
	result |= DDS_ReadRegister(DDS_FTW, dataFTW);
	printf("|-> DDS FTW : [%d] 0x%x, 0x%x 0x%x 0x%x\n", result, dataFTW[0], dataFTW[1], dataFTW[2], dataFTW[3]);

	uint8_t dataPOW[] = {0, 0};
	result |= DDS_ReadRegister(DDS_POW, dataPOW);
	printf("|-> DDS POW : [%d] 0x%x, 0x%x\n", result, dataPOW[0], dataPOW[1]);

	uint8_t dataLSP[] = {0, 0, 0, 0, 0, 0, 0, 0};
	result |= DDS_ReadRegister(DDS_LSP, dataLSP);
	printf("|-> DDS LSP : [%d] 0x%x, 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x\n", result, dataLSP[0], dataLSP[1], dataLSP[2], dataLSP[3], dataLSP[4], dataLSP[5], dataLSP[6], dataLSP[7]);

	uint8_t dataLSD[] = {0, 0, 0, 0, 0, 0, 0, 0};
	result |= DDS_ReadRegister(DDS_LSD, dataLSD);
	printf("|-> DDS LSP : [%d] 0x%x, 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x\n", result, dataLSD[0], dataLSD[1], dataLSD[2], dataLSD[3], dataLSD[4], dataLSD[5],dataLSD[6], dataLSD[7]);

	uint8_t dataLSR[] = {0, 0, 0, 0};
	result |= DDS_ReadRegister(DDS_LSR, dataLSR);
	printf("|-> DDS SRR : [%d] 0x%x 0x%x 0x%x 0x%x\n", result, dataLSR[0], dataLSR[1], dataLSR[2], dataLSR[3]);

	if(result != HAL_OK){
		printf("|-> DDS ReadRegisters fail! 0x%x\n", result);
	}else{
		printf("=-> DDS ReadRegisters FINISH\n");
	}
#endif
}


