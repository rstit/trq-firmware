const Proto = require('./protocol')

let gen = {
    batteryStructResponse() {
        return new Proto().write().ReadDataResponse({
            structId: 2,
            data: {
                voltage: 3800 + Math.random() * 400,
                avgVoltage: 3850+ Math.random() * 200,
                current: 3200+ Math.random() * 800,
                avgCurrent: 2200+ Math.random() * 800,
                percentage: 50 + Math.random() * 50,
                status: 5
            }
        }).result
    },
    analogStructResponse() {
        return new Proto().write().ReadDataResponse({
            structId: 3,
            data: {
                frequency: ((Math.random() * 5000) | 0) * 1000,
                attenuator: 1
            }
        }).result
    },
    measureData() {
        return new Proto().write().MeasurementFrame({
            type: Math.random() > 0.5 ? 1 : 2,
            timedelta: new Date() / 1000 / 1000 * 1024,
            delta: 10,
            samples: Array(20).fill(0).map(i => {
                return [Math.random() * 32000 - 32000, Math.random() * 32000 - 32000, Math.random() * 32000 - 32000].map(i => i | 0)
            })
        }).result
    }
}

function mock(frame) {
    if (input.fn == 1) {
        // read struct
        if (frame.data.structId == 2) {
            return gen.batteryStructResponse()
        }
        if (frame.data.structId == 3) {
            return gen.analogStructResponse()
        }
    }
    if (input.fn == 2) {
        // write struct
        if (frame.data.structId == 2) {
            // not allowed
        }
        if (frame.data.structId == 3) {
            return gen.analogStructResponse()
        }
    }
}
let battery = gen.batteryStructResponse()
console.log(battery)
console.log(new Proto().read(battery).Frame().result)

module.exports = mock