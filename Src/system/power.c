#include "gpio.h"
#include "periphs/all.h"
#include "wifi_interface.h"
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2 ;

void power_OnStart() {
	do {
		HAL_Delay(1);
	} while (HAL_GetTick() < 50);
	HAL_GPIO_WritePin(PWR_KEEP_GPIO_Port, PWR_KEEP_Pin, GPIO_PIN_SET);
	power_KeepAlive();
#ifdef DEBUG_PRINT
	char *ptr = "[Power] Start\n";
	printf(ptr);
#endif
	// TODO: start timeout
}

void power_Off() {
	WiFi_Status_t status = WiFi_MODULE_SUCCESS;
	printf("[Power] Close socket\n");
	status |= WIFI_ExecuteCommand("AT+S.SOCKD=0\r");
//	status |= wifi_socket_server_close();
//	status |= wifi_socket_server_open(0, "t");
	printf("[SYSTEM] PowerOff status = %d\r\n", status);
}

void power_KeepAlive() {
	deviceStatus.powerTimeout = HAL_GetTick();
#ifdef DEBUG_PRINT
	char ptr[64];
	sprintf(ptr, "[Power] KeepAlive %d\n",deviceStatus.powerTimeout);
	printf(ptr);
#endif
}
#pragma GCC push_options
#pragma GCC optimize ("O0")
void power_TimeOut() {
	if (deviceStatus.state != IDLE) {
		return;
	}
	printf("[Power] Time out!\n");
	deviceStatus.state = SLEEP;
	fflush(stdout);

	printf("[Power] Close socket\n");
	power_Off();

	printf("[Power] Power down WiFi\n");
	WIFI_PowerDown();
	{
		printf("[Power] Store data\n");
		MAX_Store();
		data_Store();
	}
	{
		printf("[Power] Turn off off\n");
		HAL_GPIO_WritePin(USER_LED_FAULT_GPIO_Port, USER_LED_FAULT_Pin, GPIO_PIN_RESET);
	//	HAL_GPIO_WritePin(PWR_KEEP_GPIO_Port, PWR_KEEP_Pin, GPIO_PIN_RESET);

		HAL_GPIO_WritePin(USER_LED_FAULT_GPIO_Port, USER_LED_FAULT_Pin, GPIO_PIN_RESET);
	//	HAL_GPIO_WritePin(PWR_KEEP_GPIO_Port, PWR_KEEP_Pin, GPIO_PIN_RESET);

		HAL_GPIO_WritePin(CHARGER_EN1_GPIO_Port, CHARGER_EN1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(CHARGER_EN2_GPIO_Port, CHARGER_EN2_Pin, GPIO_PIN_RESET);
		printf("[Power] Power Keep off\r\n");
		HAL_GPIO_WritePin(PWR_KEEP_GPIO_Port, PWR_KEEP_Pin, GPIO_PIN_RESET);
		printf("[Power] Wait for rerun\n");
	}
	HAL_GPIO_WritePin(PWR_KEEP_GPIO_Port, PWR_KEEP_Pin, GPIO_PIN_RESET);
	while(1) {
		volatile GPIO_PinState isPwrSw = HAL_GPIO_ReadPin(USER_SW_nPOWER_GPIO_Port,
				USER_SW_nPOWER_Pin);
		if (isPwrSw == GPIO_PIN_RESET) {
			power_KeepAlive();
			break;
		}
	}
	HAL_GPIO_WritePin(PWR_KEEP_GPIO_Port, PWR_KEEP_Pin, GPIO_PIN_SET);
	NVIC_SystemReset();
}
#pragma GCC pop_options
