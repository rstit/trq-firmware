/*
 * measurements.h
 *
 *  Created on: 07.05.2018
 *      Author: jkonieczny
 */

#ifndef MEASUREMENTS_H_
#define MEASUREMENTS_H_

#include "structures.h"

#ifdef __cplusplus
extern "C" {
#endif

extern MeasurementFrame adcBuffer[];
extern MeasurementFrame accBuffer[];

extern volatile MeasurementFrame *currentAdcBuffer;
extern volatile MeasurementFrame *currentAccBuffer;

/**
 * @brief Resets ADC buffer
 *
 * @param i Which buffer should be reseted
 */
void buffers_resetAdcBuffer(int i);

/**
 * @brief Resets ACC buffer
 *
 * @param i Which buffer should be reseted
 */
void buffers_resetAccBuffer(int i);

/**
 * @brief Swaps ADC buffer to next free
 *
 */
void buffers_nextAdcBuffer();

/**
 * @brief Swaps ACC buffer to next free
 */
void buffers_nextAccBuffer();

/**
 * @brief Prepare buffer to send by filling sizes
 */
void buffers_finalizeBuffer(MeasurementFrame *buffer, int items);

/**
 * @brief Add buffers to send queue and send
 */
void buffers_send(MeasurementFrame *buffer);

#ifdef __cplusplus
}
#endif

#endif /* MEASUREMENTS_H_ */
